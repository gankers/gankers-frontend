import React from "react";
import { createRemixStub } from "@remix-run/testing";
import type { Preview } from "@storybook/react";
import { themes } from "@storybook/theming";
import "../app/tailwind.css";
import "@fontsource/roboto";
import "@fontsource/roboto/900.css";
import "./preview-reset.scss";

const preview: Preview = {
  parameters: {
    docs: {
      theme: themes.dark,
    },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
  tags: ["autodocs"],
  decorators: [
    // Yoinked from https://blog.sentiero.digital/how-to-run-remix-link-component-in-storybook
    (Story) => {
      const RemixStub = createRemixStub([
        {
          path: "*",
          action: () => ({ redirect: "/" }),
          loader: () => ({ redirect: "/" }),
          Component: () => <Story />,
        },
      ]);
      return <RemixStub />;
    },
  ],
};

export default preview;
