// Replace your-framework with the framework you are using (e.g., react, vue3)
import { Preview } from "@storybook/react";
import { addons } from "@storybook/manager-api";
import { themes } from "@storybook/theming";

const preview: Preview = {
  parameters: {
    docs: {
      theme: themes.dark,
    },
  },
};
addons.setConfig({
  theme: themes.dark,
});
export default preview;
