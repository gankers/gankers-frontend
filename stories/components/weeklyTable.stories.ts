import type { Meta, StoryObj } from "@storybook/react";
import Table from "~/components/weeklyTable";
import type { WeeklyLeaderboardEntry } from "~/generated/elite_leaderboard";
import "~/styles/ggi-table.scss";
import { faker } from "@faker-js/faker";

function makeExampleData(qty: number): WeeklyLeaderboardEntry[] {
  faker.seed(1337);

  return [...new Array(qty)]
    .map(faker.internet.displayName)
    .map((cmdr_name, kills) => ({
      cmdr_name,
      kills: kills + 1,
    }))
    .reverse();
}

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
  title: "Tables/WeeklyTable",
  component: Table,
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/configure/story-layout
    layout: "centered",
  },
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ["autodocs"],
  // More on argTypes: https://storybook.js.org/docs/api/argtypes
  args: {
    weeklyData: makeExampleData(10),
  },
  // Use `fn` to spy on the onClick arg, which will appear in the actions panel once invoked: https://storybook.js.org/docs/essentials/actions#action-args
  //args: { onClick: fn() },
} satisfies Meta<typeof Table>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const TenEntries: Story = {
  args: {
    weeklyData: makeExampleData(10),
  },
};

/**
 * Info text is displayed if no entries are present
 */
export const Empty: Story = {
  args: {
    weeklyData: makeExampleData(0),
  },
};

/**
 * Only 20 Entries can be rendered at once. This should be filtered by the backend. The component will simply cap the Quantity at `20`
 */
export const TooManyEntries: Story = {
  args: {
    weeklyData: makeExampleData(30),
  },
};
