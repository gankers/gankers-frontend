import type { Meta, StoryObj } from "@storybook/react";
import { installGlobals } from "@remix-run/node";
import type { CommanderServiceType } from "~/services/cmdr.service.server";
import _CmdrDetails from "~/components/pages/elite.cmdrs.$name";

installGlobals();
// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
  title: "Pages/elite.cmdrs.$name",
  component: _CmdrDetails,
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/configure/story-layout
  },
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ["autodocs"],
  // More on argTypes: https://storybook.js.org/docs/api/argtypes
  args: {},
  // Use `fn` to spy on the onClick arg, which will appear in the actions panel once invoked: https://storybook.js.org/docs/essentials/actions#action-args
  //args: { onClick: fn() },
} satisfies Meta<typeof _CmdrDetails>;

export default meta;
type Story = StoryObj<typeof meta>;

const killEvent: Awaited<
  ReturnType<CommanderServiceType["getCmdrPage"]>
>["page_data"][number] = {
  interaction_index: "",
  kill_id: 0,
  kill_timestamp: "2024-08-18T00:13:00Z",
  killer: {
    name: "ExampleCmdr",
    rank_id: 3,
    ship_id: "viper",
    ship_name: "Viper III",
  },
  victim: {
    name: "VictimCmdr",
    rank_id: 2,
    ship_id: "python_nx",
    ship_name: "Python II",
    squadron: "TEST",
  },
  system_name: "Raxxla",
};

const deathEvent: Awaited<
  ReturnType<CommanderServiceType["getCmdrPage"]>
>["page_data"][number] = {
  interaction_index: "",
  kill_id: 0,
  system_name: "Eravate",
  kill_timestamp: "2024-08-18T00:13:00Z",
  victim: {
    name: "ExampleCmdr",
    rank_id: 3,
    ship_id: "viper",
    ship_name: "Viper III",
  },
  killer: {
    name: "KillerCmdr",
    rank_id: 2,
    ship_id: "python_nx",
    ship_name: "Python II",
    squadron: "TEST",
  },
};

/**
 * An example with no additional data
 */
export const Default: Story = {
  args: {
    result: {
      cmdr_name: "ExampleCmdr",
      deaths: 9,
      kills: 42,
      page: 1,
      page_data: [deathEvent, killEvent],
      total_pages: 1,
    },
    page: 1,
    hasMore: false,
  },
};

/**
 * An example more pages available, current commander having a Squadron
 */
export const MorePagesAndSquad: Story = {
  args: {
    result: {
      cmdr_name: "ExampleCmdr",
      squadron: "TEST",
      deaths: 9,
      kills: 42,
      page: 1,
      page_data: [deathEvent, killEvent],
      total_pages: 3,
    },
    page: 1,
    hasMore: true,
  },
};
