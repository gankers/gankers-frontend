import { json } from "@remix-run/node";
import { createRemixStub } from "@remix-run/testing";
import React from "react";

interface Props {
  loaderJson: Record<string, unknown> | undefined;
  component: React.ComponentType | null;
}

export function RemixPageStory(props: Props) {
  return createRemixStub([
    {
      path: "/",
      Component: props.component,
      loader: () => {
        return json(props.loaderJson);
      },
    },
  ]);
}
