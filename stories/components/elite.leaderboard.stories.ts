import type { Meta, StoryObj } from "@storybook/react";
import { _EliteRecent } from "~/components/pages/elite.leaderboard";
import { installGlobals } from "@remix-run/node";
import { exampleData } from "./elite.leaderboard.data";

installGlobals();
// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
  title: "Pages/elite.leaderboard",
  component: _EliteRecent,
  parameters: {},
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ["autodocs"],
  // More on argTypes: https://storybook.js.org/docs/api/argtypes
  args: {
    ...exampleData,
    result: exampleData.result.filter((_, i) => i < 10),
  },
  // Use `fn` to spy on the onClick arg, which will appear in the actions panel once invoked: https://storybook.js.org/docs/essentials/actions#action-args
  //args: { onClick: fn() },
} satisfies Meta<typeof _EliteRecent>;

export default meta;
type Story = StoryObj<typeof meta>;

/**
 * Exported example with full page, more pages present
 */
export const Default: Story = {
  args: exampleData,
};

/**
 * Empty List
 */
export const EmptySet: Story = {
  args: {
    hideEasterEgg: true, // needed otherwise we get flaky visreg tests
    hasMore: false,
    count: 0,
    result: [],
    page: 1,
  },
};
