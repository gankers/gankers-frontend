import {
  Link,
  Links,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  isRouteErrorResponse,
  useRouteError,
} from "@remix-run/react";
import "@fontsource/roboto";
import "@fontsource/roboto/900.css";

import tailwindStyles from "./tailwind.css?url";
import ggiImage from "~/assets/ggi.png?url";
import { json, type LinksFunction, type MetaFunction } from "@remix-run/node";
import environmentService from "./services/environment.service.server";
export const links: LinksFunction = () => [
  { rel: "stylesheet", href: tailwindStyles },
  { rel: "icon", href: ggiImage },
];

export const loader = () =>
  json({ host: environmentService.frontendPublicHost });

export const meta: MetaFunction<typeof loader> = ({ data }) => [
  {
    title: "Gankers.org",
  },
  {
    property: "theme-color",
    content: "#ff00ff",
  },
  {
    name: "description",
    property: "Funny number must go up. Haha ship go boom.",
  },
  {
    name: "og:title",
    content: "Gankers.org",
  },
  {
    name: "og:description",
    content: "Funny number must go up. Haha ship go boom.",
  },
  {
    name: "og:image",
    content: data!.host + "/og.png",
  },
  {
    name: "og:locale",
    content: "en_GB",
  },
];

export const ErrorBoundary = () => {
  const __error = useRouteError();
  const error = isRouteErrorResponse(__error) ? __error! : undefined;
  console.error(error);

  return (
    <div className="w-full min-h-[100vh] flex justify-center flex-col items-center bg-slate-900">
      <h1 className="text-9xl text-orange-400 font-bold">
        {error?.status ?? ":/"}
      </h1>
      <p className="text-gray-200">{error?.data}</p>
      <div className="flex gap-2 text-white">
        <Link to={"/"}>
          <button className=" bg-slate-400/20 p-2 mt-4">Back to landing</button>
        </Link>
        <Link to={"/elite"}>
          <button className=" bg-slate-400/20 p-2 mt-4">Back to Elite</button>
        </Link>
      </div>
    </div>
  );
};

export function Layout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:type" content="website" />
        <Links />
        <Meta />
      </head>
      <body className=" w-full min-h-[100vh] flex justify-center flex-col">
        {children}
        <ScrollRestoration />
        <Scripts />
      </body>
    </html>
  );
}

export default function App() {
  return <Outlet />;
}
