import { Outlet } from "@remix-run/react";

export default function Weekly() {
  return <Outlet />;
}
