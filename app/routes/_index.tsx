import type { LinksFunction, MetaFunction } from "@remix-run/node";

import eliteCard from "app/assets/landing/example_elite.webp?url";
import eliteSvg from "app/assets/landing/ed.svg?url";
import scSvg from "app/assets/landing/sc.svg?url";
import scCard from "app/assets/landing/sc_card.jpg?url";
import indexStyles from "../styles/index.scss?url";
import {
  Link,
  isRouteErrorResponse,
  redirect,
  useRouteError,
} from "@remix-run/react";

export const meta: MetaFunction = () => {
  return [
    { title: "Galactic Gank Initiative · Game Selection" },
    {
      name: "description",
      content: "Galactic Gank Initiative · Game Selection",
    },
  ];
};

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: indexStyles },
];

export function ErrorBoundary() {
  const __error = useRouteError();
  const error = isRouteErrorResponse(__error) ? __error! : undefined;
  console.error(error);

  if (error?.status === 404) {
    return (
      <div>
        <h1>
          <span className=" text-orange-400 text-9xl font-black">404</span>
        </h1>
        <p className="text-white text-3xl">Route not found</p>
        <Link to={"/elite"}>
          <button className=" bg-slate-400/20 p-2 mt-4">
            Back to Elite main page
          </button>
        </Link>
      </div>
    );
  }

  return (
    <div>
      <h1>
        <span className=" text-orange-400 text-9xl font-black">
          {error?.status ?? ":/"}
        </span>
      </h1>
      <p className="text-white text-3xl">{error?.statusText}</p>
      <Link to={"/elite"}>
        <button className=" bg-slate-400/20 p-2 mt-4">
          Back to Elite main page
        </button>
      </Link>
    </div>
  );
}

export const loader = () => {
  return redirect("/elite");
};

export default function Index() {
  return (
    <div className=" bg-slate-950 w-full max-h-full items-center  min-h-full flex flex-row justify-center flex-1  ">
      <div className="flex flex-row gap-3 self-center">
        <Link
          to={"./elite"}
          id="card-elite"
          className="landing-card relative overflow-hidden group rounded-lg"
        >
          <div
            aria-description="card-backdrop"
            className="overflow-hidden bg-slate-600 aspect-[9/16] w-96 flex justify-stretch items-stretch"
          >
            <img
              src={eliteCard}
              alt=""
              className="hover:scale-110 brightness-100 hover:brightness-50  transition duration-500 cursor-pointer object-cover block saturate-100 hover:saturate-0"
            />
          </div>
          <div className=" absolute top-0 left-[-100%] duration-300 group-hover:left-0 max-h-fill pointer-events-none p-2 w-full h-full">
            <h1 className="font-black text-white text-8xl uppercase ">
              Elite Dangerous
            </h1>
          </div>
          <img
            src={eliteSvg}
            alt=""
            className="w-32 h-32 absolute bottom-0 left-[50%] -ml-16 mb-5 opacity-100 group-hover:opacity-0 pointer-events-none duration-300"
          />
        </Link>

        <Link
          to={"./star-citizen"}
          id="card-sc"
          className="landing-card relative overflow-hidden group rounded-lg"
        >
          <div
            aria-description="card-backdrop"
            className="overflow-hidden bg-slate-600 aspect-[9/16] w-96 flex justify-stretch items-stretch"
          >
            <img
              src={scCard}
              alt=""
              className="hover:scale-110 brightness-100 hover:brightness-50 transition duration-500 cursor-pointer object-cover block saturate-100 hover:saturate-0"
            />
          </div>
          <div className=" absolute top-0 left-[-100%] duration-300 group-hover:left-0 max-h-fill pointer-events-none p-2 w-full h-full">
            <h1 className="font-black text-white text-8xl uppercase ">
              (WIP)
              <br />
              Star Citizen
            </h1>
          </div>
          <img
            src={scSvg}
            alt=""
            className="w-32 h-32 absolute bottom-0 left-[50%] -ml-16 mb-5 opacity-100 group-hover:opacity-0 pointer-events-none duration-300"
          />
        </Link>
      </div>
    </div>
  );
}
