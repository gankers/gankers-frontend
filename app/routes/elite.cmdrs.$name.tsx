import {
  LinksFunction,
  LoaderFunctionArgs,
  MetaFunction,
  json,
} from "@remix-run/node";
import {
  Link,
  isRouteErrorResponse,
  useLoaderData,
  useParams,
  useRouteError,
} from "@remix-run/react";
import { z } from "zod";
import ggiTables from "~/styles/ggi-table.scss?url";
import ggiPagination from "~/styles/ggi-pagination.scss?url";
import _CmdrDetails from "~/components/pages/elite.cmdrs.$name";
import graphsServiceServer from "~/services/graphs.service.server";
import cmdrServiceServer from "~/services/cmdr.service.server";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: ggiTables },
  { rel: "stylesheet", href: ggiPagination },
];

export const meta: MetaFunction<typeof loader> = ({ data, matches }) => {
  const parentMeta = matches[0]?.meta ?? [];
  if (!data?.result) {
    return [...parentMeta];
  }
  const title = `${data.result.squadron ? "[" + data.result.squadron + "] " : ""}${data.result.cmdr_name}`;
  const description = `CMDR ${data.result.cmdr_name} racked in ${data.result.kills} Kills and ${data.result.deaths} Deaths.`;

  return [
    ...parentMeta.filter(
      (e) =>
        ("name" in e && (e.name === "og:image" || e.name === "og:locale")) ||
        ("property" in e && e.property === "theme-color"),
    ),
    { title: title },
    {
      name: "description",
      property: description,
    },
    {
      name: "og:description",
      content: description,
    },
    {
      name: "og:title",
      content: title,
    },
  ];
};

export const loader = async ({ params, request }: LoaderFunctionArgs) => {
  const cmdrName = params["name"];
  const unsafePage = new URL(request.url).searchParams.get("page") ?? "1";
  const pageParseResult = z
    .number()
    .min(1)
    .int()
    .default(1)
    .safeParse(Number(unsafePage));
  const page = pageParseResult.success ? pageParseResult.data : 1;

  if (!cmdrName) {
    throw new Response("No CMDR provided?", { status: 400 });
  }

  const [result, graphHistory] = await Promise.all([
    cmdrServiceServer.getCmdrPage(cmdrName, page),
    graphsServiceServer.getWeeklySeriesForCMDR(cmdrName),
  ]);

  return json({ result, page, graphHistory });
};

export function ErrorBoundary() {
  const __error = useRouteError();
  const error = isRouteErrorResponse(__error) ? __error! : undefined;
  const cmdr = useParams()["name"];
  console.error(error);

  if (error?.status === 404) {
    return (
      <div>
        <h1>
          <span className=" text-orange-400 text-9xl font-black">404</span>
        </h1>
        <p className="text-white text-3xl">
          CMDR <span className="text-orange-400 font-black">{cmdr}</span>?
          Don&apos;t know em.
        </p>
        <Link to={"/elite"}>
          <button className=" bg-slate-400/20 p-2 mt-4">
            Back to main page
          </button>
        </Link>
      </div>
    );
  }

  return (
    <div>
      <h1>
        <span className=" text-orange-400 text-9xl font-black">
          {error?.status ?? ":/"}
        </span>
      </h1>
      <p className="text-white text-3xl">{error?.statusText}</p>
      <Link to={"/elite"}>
        <button className=" bg-slate-400/20 p-2 mt-4">Back to main page</button>
      </Link>
    </div>
  );
}

export default function CmdrDetails() {
  const { result, page, graphHistory } = useLoaderData<typeof loader>();
  const hasMore = page < result.total_pages;

  return (
    <_CmdrDetails
      graphHistory={graphHistory}
      hasMore={hasMore}
      page={page}
      result={result}
    />
  );
}
