import { Link } from "@remix-run/react";
import EliteImg from "~/assets/landing/ed.svg";

export default function Landing() {
  return (
    <div>
      <h1 className="text-4xl">The State of Star Citizen Integration</h1>
      <p className="my-2">
        At this point the infrastructure is there to enable a kill tracker for
        Star Citizen. Only issue as of now is that SC does not provide any API /
        Log we could interact with to get a reading of Kills / Deaths. There is
        no interest in providing a service where one would have to enter kills
        by hand.
      </p>
      <p className="mt-2 mb-5">
        If SC provides a mechanism, be it an API or a Journal like Elite,
        functionality similar to Elite&lsquo;s Integration could be implemented
        with relative ease and haste.
      </p>
      <div>
        <Link
          className="max-w-fit text-xl p-2 flex items-center justify-start gap-2 bg-slate-800 hover:bg-orange-600"
          to={"/elite"}
        >
          <img className="w-8 h-8" src={EliteImg} alt="" />
          Go to Elite
        </Link>
      </div>
    </div>
  );
}
