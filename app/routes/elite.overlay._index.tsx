import React, { useEffect } from "react";
import { z } from "zod";
import OverlayConfigurer, {
  OverlayConfigZod,
} from "~/components/overlay/configurer";
import OverlayPreview from "~/components/overlay/preview";

export default function OverlayLanding() {
  const [configState, setConfigState] = React.useState<
    z.infer<typeof OverlayConfigZod> | undefined
  >();

  useEffect(() => {
    // Initial setup. Loading from localstorage.
    // This MUST be inside a useEffect because otherwise Remix
    // would try to prerender and access window.localStorage,
    // which doesnt exist on the server!
    let configString = window.localStorage.getItem("OVERLAY_CONFIG");
    if (configString === null) {
      configString = "{}";
    }
    try {
      const parsed = JSON.parse(configString);
      const config = OverlayConfigZod.parse(parsed);
      setConfigState(config);
    } catch (err) {
      console.warn("failed to parse the overlay config. Reverting to default");
    }
  }, []);

  useEffect(() => {
    // Sync any Config Changes to localStorage
    if (!configState) {
      return;
    }
    window.localStorage.setItem("OVERLAY_CONFIG", JSON.stringify(configState));
  }, [configState]);

  const currentConfigB64String = btoa(JSON.stringify(configState));

  const buttonLinkStyles =
    "bg-slate-800 border-r border-slate-700 p-2 hover:bg-orange-800 duration-150";

  return (
    <div className=" flex flex-col gap-12 flex-1 ">
      <section
        id="preview"
        className="bg-slate-950 border border-slate-700 rounded-lg"
      >
        <div className="p-1">
          <h1 className="text-4xl">Preview</h1>
          <p className="text-sm text-neutral-300">
            This is what your configured overlay will look like
          </p>
        </div>
        {configState && <OverlayPreview config={configState} />}
        <div className=" flex flex-row">
          <a
            className={`${buttonLinkStyles} rounded-bl-lg`}
            target="_blank"
            rel="noreferrer"
            href={`/elite/overlay/${currentConfigB64String}`}
          >
            Open Live Overlay
          </a>
          <button
            onClick={() => {
              const currentUrl = `${window.location.origin}/elite/overlay/${currentConfigB64String}`;
              navigator.clipboard.writeText(currentUrl);
            }}
            className={`${buttonLinkStyles}`}
            rel="noreferrer"
          >
            Copy Config Link
          </button>
          <a
            className={`${buttonLinkStyles}`}
            target="_blank"
            rel="noreferrer"
            href={`/elite/overlay/${currentConfigB64String}?demo`}
          >
            Open Demo Overlay
          </a>
        </div>
      </section>
      <section id="settings" className="">
        <h1 className="text-4xl">Configuration</h1>
        <p className="text-sm text-neutral-300 mb-4">
          Make the overlay your very own. These settings are stored in local
          storage. If you close this page and come back they should persist :)
        </p>
        {configState && (
          <OverlayConfigurer
            setCurrentConfig={setConfigState}
            currentConfig={configState}
          ></OverlayConfigurer>
        )}
      </section>
      <div className="h-48" />
    </div>
  );
}
