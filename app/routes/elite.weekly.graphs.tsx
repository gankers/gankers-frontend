import { json, LoaderFunctionArgs } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { useMemo } from "react";
import {
  SummaryRankGraph,
  SummaryShipGraph,
  SummarySystemsGraph,
} from "~/components/graphs/SummaryGraphEntry";
import {
  TimedRankGraph,
  TimedShipGraph,
  TimedSystemGraph,
} from "~/components/graphs/TimedGroupedGraph";
import { getWeekOfYear } from "~/components/graphs/util";
import graphsServiceServer from "~/services/graphs.service.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const url = new URL(request.url);
  const timestamp = url.searchParams.get("ts") as string | undefined;

  const [top10, total, systems, ships, ranks, summary] = await Promise.all([
    graphsServiceServer.getWeeklyProgress(timestamp),
    graphsServiceServer.getWeekHourlySeries(timestamp),
    graphsServiceServer.getWeekHourlySeriesSystems(timestamp),
    graphsServiceServer.getWeekHourlySeriesShips(timestamp),
    graphsServiceServer.getWeekHourlySeriesRanks(timestamp),
    graphsServiceServer.getWeekSummary(timestamp),
  ]);

  return json({
    top10,
    total,
    systems,
    ships,
    ranks,
    summary,
  });
};

export default function Graphs() {
  const data = useLoaderData<typeof loader>();
  const week = getWeekOfYear(new Date(data.total[0].time));

  const ships = useMemo(() => {
    const sorted = [...data.summary.ships].sort((a, b) => {
      const aValue = a.deaths > a.kills ? a.deaths : a.kills;
      const bValue = b.deaths > b.kills ? b.deaths : b.kills;
      return bValue - aValue;
    });

    const relevantData = sorted.filter((_, i) => i < 10);
    const groupedData = sorted
      .filter((_, i) => i >= 10)
      .reduce(
        (prev, next) => {
          const response = {
            ...prev,
          };
          response.deaths += next.deaths;
          response.kills += next.kills;

          return response;
        },
        {
          deaths: 0,
          kills: 0,
          ship_color: "#aaa",
          ship_id: "other",
          ship_name: "Other",
        },
      );
    return [...relevantData, groupedData];
  }, [data]);

  const systems = useMemo(() => {
    const sorted = [...data.summary.systems].sort((a, b) => {
      return b.kills - a.kills;
    });

    const relevantData = sorted.filter((_, i) => i < 10);
    const groupedData = sorted
      .filter((_, i) => i >= 10)
      .reduce(
        (prev, next) => {
          const response = {
            ...prev,
          };
          response.kills += next.kills;
          return response;
        },
        {
          kills: 0,
          system_name: "Other",
        },
      );
    return [...relevantData, groupedData];
  }, [data]);

  const ranks = data.summary.ranks.filter((e) => e.rank < 10);

  const relevantSystems = systems.map((x) => x.system_name);
  const relevantShips = ships.map((x) => x.ship_name);
  const timedRanks = data.ranks.filter((e) => e.rank < 10);
  const timedShips = data.ships.filter((e) =>
    relevantShips.includes(e.ship_name),
  );
  const timedSystems = data.systems.filter((e) =>
    relevantSystems.includes(e.system_name),
  );

  return (
    <div className="flex flex-col w-full px-2 max-w-screen-2xl py-12">
      <h1 className="text-4xl">{week + "."} Week in numbers</h1>

      <h2 className="text-xl mt-5 mb-2">System Statistic</h2>
      <div className="flex flex-col md:flex-row">
        <TimedSystemGraph data={timedSystems} />
        <SummarySystemsGraph data={systems} widthOverride={512} />
      </div>
      <h2 className="text-xl mt-5 mb-2">Ship Statistic</h2>
      <div className="flex flex-col md:flex-row">
        <TimedShipGraph data={timedShips} />
        <SummaryShipGraph data={ships} widthOverride={512} />
      </div>
      <h2 className="text-xl mt-5 mb-2">Rank Statistic</h2>
      <div className="flex flex-col md:flex-row">
        <TimedRankGraph data={timedRanks} />
        <SummaryRankGraph data={ranks} widthOverride={512} />
      </div>
    </div>
  );
}
