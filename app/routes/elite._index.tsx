import { LinksFunction, json } from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";
import WeeklyGroupedGraph from "~/components/graphs/WeeklyGroupedGraph";
import LeaderboardTable from "~/components/leaderboardTable";
import RecentKillsTable from "~/components/recentKillsTable";
import WeeklyLeaderboardTable from "~/components/weeklyTable";
import graphsServiceServer from "~/services/graphs.service.server";
import leaderboardServiceServer from "~/services/leaderboard.service.server";
import recentKillsServiceServer from "~/services/recentKills.service.server";
import ggiTables from "~/styles/ggi-table.scss?url";

export const loader = async () => {
  const [
    leaderboardData,
    recentData,
    weeklyLeaderboardData,
    historicalWeeklyData,
  ] = await Promise.all([
    leaderboardServiceServer
      .getLeaderboardPage(1)
      .then(({ result }) => result.filter((_, i) => i < 20)),
    recentKillsServiceServer.getPage(1, 20),
    leaderboardServiceServer.getWeeklyLeaderboardPage(1, 10),
    graphsServiceServer.getWeeklyGrouped(),
  ]);

  return json({
    leaderboardData,
    recentData,
    weeklyLeaderboardData,
    historicalWeeklyData,
  });
};

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: ggiTables },
];

export default function EliteIndex() {
  const data = useLoaderData<typeof loader>();

  return (
    <div className=" w-full max-w-screen-xl">
      <div className="w-full flex flex-col p-4 ">
        <WeeklyGroupedGraph data={data.historicalWeeklyData} />
      </div>
      <div className="w-full px-2 flex flex-col mt-12 md:mt-0 md:flex-row gap-3 mb-5">
        <section id="mostRecentEntries" className="flex-1 max-w-full">
          <h2 className="text-3xl font-black pb-5">Most recent kills</h2>
          <RecentKillsTable recentData={data.recentData} />
          <Link to={"./recent"}>
            <div className="text-lg bg-slate-800 p-3 border-b-2 hover:bg-slate-600 border-orange-500">
              View entire History
            </div>
          </Link>
        </section>

        <section id="leaderBoard">
          <h2 className="text-3xl font-black  pb-5">Leaderboard</h2>
          <LeaderboardTable leaderboardData={data.leaderboardData} />
          <Link to={"./leaderboard"}>
            <div className="text-lg bg-slate-800 p-3 border-b-2 hover:bg-slate-600 border-orange-500">
              View entire Leaderboard
            </div>
          </Link>

          <h2 className="text-3xl font-black  mt-12 pb-5">Weekly Board</h2>
          <WeeklyLeaderboardTable weeklyData={data.weeklyLeaderboardData} />

          <Link to={"./weekly"}>
            <div className="text-lg bg-slate-800 p-3 border-b-2 hover:bg-slate-600 border-orange-500">
              View entire weekly board
            </div>
          </Link>

          <Link className="" to={"./submit"}>
            <div className="text-lg mt-5 bg-slate-800 p-3 hover:bg-slate-600">
              Upload your kills
            </div>
          </Link>

          <Link className="" to={"./overlay"}>
            <div className="text-lg mt-5 bg-slate-800 p-3 hover:bg-slate-600">
              Setup Kill Overlay
            </div>
          </Link>
        </section>
      </div>
    </div>
  );
}
