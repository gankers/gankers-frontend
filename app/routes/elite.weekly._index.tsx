import { LinksFunction, LoaderFunctionArgs, json } from "@remix-run/node";
import { Link, useLoaderData, useSearchParams } from "@remix-run/react";
import { z } from "zod";
import ggiTables from "~/styles/ggi-table.scss?url";
import ggiPagination from "~/styles/ggi-pagination.scss?url";
import leaderboardServiceServer from "~/services/leaderboard.service.server";
import WeeklyLeaderboardTable from "~/components/weeklyTable";
import graphsServiceServer from "~/services/graphs.service.server";
import WeekHourlyGroupedGraph from "~/components/graphs/WeekHourlyGroupedGraph";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: ggiTables },
  { rel: "stylesheet", href: ggiPagination },
];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const url = new URL(request.url);
  const timestamp = url.searchParams.get("ts") ?? "";
  const unsafePage = new URL(request.url).searchParams.get("page") ?? "1";
  const pageParseResult = z
    .number()
    .min(1)
    .int()
    .default(1)
    .safeParse(Number(unsafePage));
  const count = 50;
  const page = pageParseResult.success ? pageParseResult.data : 1;

  const [result, weeklyProgressData, weekHourlyProgressData] =
    await Promise.all([
      leaderboardServiceServer.getWeeklyLeaderboardPage(page, count),
      graphsServiceServer.getWeeklyProgress(),
      graphsServiceServer.getWeekHourlySeries(timestamp),
    ]);

  return json({
    result,
    hasMore: result.length == count,
    weeklyProgressData,
    weekHourlyProgressData,
  });
};

export default function EliteRecent() {
  const { result, hasMore, weekHourlyProgressData } =
    useLoaderData<typeof loader>();
  const [searchParams] = useSearchParams();

  const page = Number(searchParams.get("page") ?? "1");

  const PrevBtn = () => <button disabled={page < 2}>Previous Page</button>;

  const NextBtn = () => <button disabled={!hasMore}>Next Page</button>;

  return (
    <div className="w-full max-w-screen-lg">
      <div className="w-full flex flex-col mb-12 ">
        <WeekHourlyGroupedGraph data={weekHourlyProgressData} />
        <Link className="mt-5" to={"./graphs"}>
          <div className="text-lg bg-slate-800 p-3 border-b-2 hover:bg-slate-600 border-orange-500">
            More statistics
          </div>
        </Link>
      </div>
      <div className="flex flex-col w-full items-center gap-3 mb-5">
        <h1 className="w-full text-4xl">Weekly Leaderboard</h1>
        <p className="w-full text-neutral-300 -mt-3">
          This board gets reset every Thursday at 8AM UTC
        </p>
        <WeeklyLeaderboardTable offset={(page - 1) * 50} weeklyData={result} />
        <div className="flex flex-row gap-3 pagination-group">
          {page < 2 ? (
            <PrevBtn />
          ) : (
            <Link to={{ search: "page=" + (page - 1) }}>
              <PrevBtn />
            </Link>
          )}
          <div>{page}</div>
          {!hasMore ? (
            <NextBtn />
          ) : (
            <Link to={{ search: "page=" + (page + 1) }}>
              <NextBtn />
            </Link>
          )}
        </div>
      </div>
    </div>
  );
}
