import {
  LinksFunction,
  LoaderFunctionArgs,
  MetaFunction,
  json,
} from "@remix-run/node";
import {
  Link,
  isRouteErrorResponse,
  useLoaderData,
  useParams,
  useRouteError,
} from "@remix-run/react";
import { z } from "zod";
import ggiTables from "~/styles/ggi-table.scss?url";
import ggiPagination from "~/styles/ggi-pagination.scss?url";
import _SystemDetails from "~/components/pages/elite.systems.$system";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: ggiTables },
  { rel: "stylesheet", href: ggiPagination },
];

export const meta: MetaFunction<typeof loader> = ({ data, matches }) => {
  const parentMeta = matches[0]?.meta ?? [];
  if (!data?.result) {
    return [...parentMeta];
  }
  const title = data.result.meta.system_name;
  const description = `${data.result.meta.kill_count} CMDRs were relieved of their rebuy here.`;

  return [
    ...parentMeta.filter(
      (e) =>
        ("name" in e && (e.name === "og:image" || e.name === "og:locale")) ||
        ("property" in e && e.property === "theme-color"),
    ),
    { title: title },
    {
      name: "description",
      property: description,
    },
    {
      name: "og:description",
      content: description,
    },
    {
      name: "og:title",
      content: title,
    },
  ];
};

export const loader = async ({ params, request }: LoaderFunctionArgs) => {
  const systemName = params["system"];
  const unsafePage = new URL(request.url).searchParams.get("page") ?? "1";
  const pageParseResult = z
    .number()
    .min(1)
    .int()
    .default(1)
    .safeParse(Number(unsafePage));
  const page = pageParseResult.success ? pageParseResult.data : 1;

  if (!systemName) {
    throw new Response("No CMDR provided?", { status: 400 });
  }

  const result = await import("~/services/system.service.server").then((e) =>
    e.default.getSystemPage(systemName, page),
  );

  return json({ result, page });
};

export function ErrorBoundary() {
  const __error = useRouteError();
  const error = isRouteErrorResponse(__error) ? __error! : undefined;
  const system = useParams()["system"];
  console.error(error);

  if (error?.status === 404) {
    return (
      <div>
        <h1>
          <span className=" text-orange-400 text-9xl font-black">404</span>
        </h1>
        <p className="text-white text-3xl">
          <span className="text-orange-400 font-black">{system}</span>? Maybe
          that&apos;s where Raxxla is hiding.
        </p>
        <Link to={"/elite"}>
          <button className=" bg-slate-400/20 p-2 mt-4">
            Back to main page
          </button>
        </Link>
      </div>
    );
  }

  return (
    <div>
      <h1>
        <span className=" text-orange-400 text-9xl font-black">
          {error?.status ?? ":/"}
        </span>
      </h1>
      <p className="text-white text-3xl">{error?.statusText}</p>
      <Link to={"/elite"}>
        <button className=" bg-slate-400/20 p-2 mt-4">Back to main page</button>
      </Link>
    </div>
  );
}

export default function CmdrDetails() {
  const { result, page } = useLoaderData<typeof loader>();
  const hasMore = page < result.total_pages;

  return <_SystemDetails hasMore={hasMore} page={page} result={result} />;
}
