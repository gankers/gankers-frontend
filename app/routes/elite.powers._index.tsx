import { json, LoaderFunctionArgs } from "@remix-run/node";
import { Link, useLoaderData, useSearchParams } from "@remix-run/react";
import { useMemo } from "react";
import { getPowerPortait } from "~/assets/powerPortraits/powerPortraits";
import { MdiBulldozer } from "~/components/icons/bulldozer";
import { DeathIcon, KillIcon, PersonIcon } from "~/components/icons/kill_icons";
import powerColours from "~/components/icons/powers/powerColors";

import { PowerIcon } from "~/components/icons/powers/power_icon";
import ConsiderUnpledgeToggle from "~/components/powerplay/unpledgedToggle";
import powersService from "~/services/powers.service.server";

export const loader = async ({ params, request }: LoaderFunctionArgs) => {
  const url = new URL(request.url);
  const withUnpledged = url.searchParams.get("withUnpledged") === "true";
  const powersSummary = (
    await powersService.getPowersPageOverviewData(withUnpledged)
  )
    .filter((e) => e.name !== "Zachary Hudson")
    .sort((a, b) => {
      if (a.week.kills === b.week.kills) {
        return a.name.localeCompare(b.name);
      }
      return b.week.kills - a.week.kills;
    });

  return json({ powersSummary });
};

export default function PowersIndex() {
  const data = useLoaderData<typeof loader>();
  const [searchParam, setSearchParams] = useSearchParams();
  const considerUnpledgedCmdrs = useMemo(() => {
    const entry = searchParam.get("withUnpledged");
    return entry === "true";
  }, [searchParam]);

  const toggleShouldConsiderUnpledgedCmdrs = () => {
    setSearchParams((prev) => {
      if (considerUnpledgedCmdrs) {
        prev.delete("withUnpledged");
      } else {
        prev.set("withUnpledged", "true");
      }
      return prev;
    });
  };

  return (
    <div className="w-full flex flex-col items-center">
      <div className="flex flex-row gap-2 w-full max-w-screen-lg items-end mb-2">
        <ConsiderUnpledgeToggle
          onChangeCallback={toggleShouldConsiderUnpledgedCmdrs}
          checked={considerUnpledgedCmdrs}
        />
      </div>
      <section
        id="graphs"
        className="p-2 inline-flex text-orange-300 flex-col w-full items-center gap-4 bg-orange-800/20 max-w-screen-md mb-2"
      >
        <p className="text-xl ">This is still under construction</p>
        <MdiBulldozer className="w-12 h-12" />
      </section>
      <div className="flex flex-col gap-3 mb-12 w-full max-w-screen-lg">
        {data.powersSummary.map((e) => (
          <Link
            className="relative flex flex-row gap-2 w-full items-center border rounded-xl overflow-clip "
            to={{
              pathname: "/elite/powers/" + e.name,
              search: considerUnpledgedCmdrs ? "?withUnpledged=true" : "",
            }}
            style={{
              color: powerColours[e.name],
              background: powerColours[e.name] + "22",
              borderColor: powerColours[e.name],
            }}
            key={e.name}
          >
            <PowerIcon
              className=" w-96 absolute right-0 rotate-12 opacity-40"
              power={e.name}
            />
            <img
              className="w-32 z-10 h-32"
              src={getPowerPortait(e.name)}
              alt={"Portrait of " + e.name}
            />
            <div className="flex flex-col px-3">
              <p className="text-2xl">{e.name}</p>
              <span className="flex flex-row gap-2 text-4xl">
                <div className="flex flex-row gap-2 lg:gap-12">
                  {/* Weekly Info */}
                  <div
                    style={{ borderColor: powerColours[e.name] }}
                    className="flex flex-col"
                  >
                    <div className="flex gap-4 flex-row text-lg lg:text-3xl ">
                      <div
                        title="Kills this week by agents of this power"
                        className="flex-col"
                      >
                        <KillIcon />
                        <span>{e.week.kills}</span>
                      </div>
                      <div
                        title="Deaths this week by agents of this power"
                        className="flex-col"
                      >
                        <DeathIcon />
                        <span>{e.week.deaths}</span>
                      </div>
                      <div
                        title="Agents with kills or deaths for this power"
                        className="flex-col"
                      >
                        <PersonIcon />
                        <span>{e.week.agents ?? "N/A"}</span>
                      </div>
                    </div>
                    <div
                      style={{ background: powerColours[e.name] }}
                      className="w-full h-1"
                    ></div>
                    <p className="text-lg">Weekly Data</p>
                  </div>
                  {/* Global Info */}
                  <div
                    style={{ borderColor: powerColours[e.name] }}
                    className="flex flex-col"
                  >
                    <div className="flex gap-4 flex-row text-lg lg:text-3xl ">
                      <div
                        title="All-time Kills by agents of this power"
                        className="flex-col"
                      >
                        <KillIcon />
                        <span>{e.total.kills}</span>
                      </div>
                      <div
                        title="All-time Deaths by agents of this power"
                        className="flex-col"
                      >
                        <DeathIcon />
                        <span>{e.total.deaths}</span>
                      </div>
                    </div>
                    <div
                      style={{ background: powerColours[e.name] }}
                      className="w-full h-1"
                    ></div>
                    <p className="text-lg">All Data</p>
                  </div>
                </div>
              </span>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
}
