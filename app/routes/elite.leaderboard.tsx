import { LinksFunction, LoaderFunctionArgs, json } from "@remix-run/node";
import { useLoaderData, useSearchParams } from "@remix-run/react";
import { z } from "zod";
import ggiTables from "~/styles/ggi-table.scss?url";
import ggiPagination from "~/styles/ggi-pagination.scss?url";
import { _EliteRecent } from "~/components/pages/elite.leaderboard";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: ggiTables },
  { rel: "stylesheet", href: ggiPagination },
];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const unsafePage = new URL(request.url).searchParams.get("page") ?? "1";
  const pageParseResult = z
    .number()
    .min(1)
    .int()
    .default(1)
    .safeParse(Number(unsafePage));
  const count = 50;
  const page = pageParseResult.success ? pageParseResult.data : 1;

  const { result } = await import("~/services/leaderboard.service.server").then(
    (e) =>
      e.default.getLeaderboardPage(
        page,
        //count,
      ),
  );
  return json({ result: result, hasMore: count == result.length, count });
};

export default function EliteRecent() {
  const { result, hasMore, count } = useLoaderData<typeof loader>();
  const [searchParams] = useSearchParams();

  const page = Number(searchParams.get("page") ?? "1");
  return (
    <_EliteRecent result={result} hasMore={hasMore} count={count} page={page} />
  );
}
