import { json, LinksFunction, LoaderFunctionArgs } from "@remix-run/node";
import {
  Link,
  useLoaderData,
  useParams,
  useSearchParams,
} from "@remix-run/react";
import { useMemo } from "react";
import powerColours from "~/components/icons/powers/powerColors";
import LeaderboardTable from "~/components/leaderboardTable";
import ConsiderUnpledgeToggle from "~/components/powerplay/unpledgedToggle";
import PowerSummary from "~/components/powerplay/powerDetailPageHeader";
import RecentKillsTable from "~/components/recentKillsTable";
import powersService from "~/services/powers.service.server";
import powerOverrides from "~/styles/power-overrides.scss?url";
import { MdiBulldozer } from "~/components/icons/bulldozer";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: powerOverrides },
];

export const loader = async ({ params, request }: LoaderFunctionArgs) => {
  const powerName = params["power"];
  const url = new URL(request.url);
  const withUnpledged = url.searchParams.get("withUnpledged") === "true";
  const [powerInfo, agentInfo, recentData] = await Promise.all([
    powersService.getPowerDetailInfo(powerName!, withUnpledged),
    powersService.getAgentLeaderboard(powerName!, 1, 20, withUnpledged),
    powersService.getRecentPowerKills(powerName!, 1, 20, withUnpledged),
  ]);
  if (powerInfo === null || recentData === null) {
    throw new Response("Power " + powerName + "does not exists", {
      status: 404,
    });
  }

  return json({
    powerInfo,
    agentInfo: (agentInfo ?? []).map((e) => ({
      cmdr_name: e.name,
      kills: e.kills,
      deaths: e.deaths,
    })),
    recentData,
  });
};
export default function PowersIndex() {
  const { powerInfo, agentInfo, recentData } = useLoaderData<typeof loader>();
  const power = useParams()["power"] ?? "";
  const colour = powerColours[power!];
  const bgColour = colour + "22";
  const [searchParam, setSearchParams] = useSearchParams();

  const considerUnpledgedCmdrs = useMemo(() => {
    const entry = searchParam.get("withUnpledged");
    return entry === "true";
  }, [searchParam]);

  const toggleShouldConsiderUnpledgedCmdrs = () => {
    setSearchParams((prev) => {
      if (considerUnpledgedCmdrs) {
        prev.delete("withUnpledged");
      } else {
        prev.set("withUnpledged", "true");
      }
      return prev;
    });
  };

  return (
    <div className="w-full flex flex-col items-center">
      <div className="w-full h-0 sm:h-16 " />
      <PowerSummary
        bgColour={bgColour}
        colour={colour}
        power={power}
        powerInfo={powerInfo}
      />
      <section id="filter_settings" className="p-2 w-full sm:px-12">
        <ConsiderUnpledgeToggle
          onChangeCallback={toggleShouldConsiderUnpledgedCmdrs}
          checked={considerUnpledgedCmdrs}
        />
      </section>
      <section
        id="graphs"
        className="p-2 inline-flex text-orange-300 flex-col w-full items-center gap-4 bg-orange-800/20 max-w-screen-md mb-2"
      >
        <p className="text-xl ">This is still under construction</p>
        <MdiBulldozer className="w-12 h-12" />
      </section>
      <section
        id=""
        className="w-full mb-12 px-12 flex flex-col lg:flex-row gap-12"
      >
        <section
          id="mostRecentEntries"
          className="flex-1 max-w-full w-full flex flex-col"
        >
          <h2 className="text-3xl font-black">Most recent kills</h2>
          {!considerUnpledgedCmdrs && (
            <p className=" text-sm text-white/50 pb-5">
              Only kills where both parties are pledged to a Power are
              considered
            </p>
          )}
          <RecentKillsTable recentData={recentData} />
          <Link to={"./recent"}>
            <div className="text-lg bg-slate-800 p-3 border-b-2 hover:bg-slate-600 border-orange-500">
              View entire History
            </div>
          </Link>
        </section>
        <div>
          <h2 className="text-3xl font-black">Top Agents for this Week</h2>
          {!considerUnpledgedCmdrs && (
            <p className=" text-sm text-white/50 pb-5">
              Only kills where both parties are pledged to a Power are
              considered
            </p>
          )}
          <LeaderboardTable offset={0} leaderboardData={agentInfo} />
        </div>
      </section>
    </div>
  );
}
