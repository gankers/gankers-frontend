import { LoaderFunctionArgs, json } from "@remix-run/node";
import { Form, Link, useLoaderData, useSearchParams } from "@remix-run/react";
import cmdrService from "~/services/cmdr.service.server";

export const loader = async (args: LoaderFunctionArgs) => {
  const url = new URL(args.request.url);
  const searchParam = url.searchParams.get("q");

  if (!searchParam) {
    return json({ result: [] });
  }

  const response = await cmdrService.searchCmdrsByName(searchParam);
  return json({ result: response });
};

export default function Search() {
  const { result } = useLoaderData<typeof loader>();
  const [, setSearchParams] = useSearchParams();

  return (
    <Form className=" flex flex-col w-full" method="get" replace>
      <input
        onChange={(ev) => {
          const val = ev.target.value;
          if (!val) {
            return;
          }
          setSearchParams(new URLSearchParams({ q: val }));
        }}
        // eslint-disable-next-line jsx-a11y/no-autofocus
        autoFocus
        // eslint-disable-next-line jsx-a11y/tabindex-no-positive
        tabIndex={10}
        min={2}
        name="q"
        style={{ outline: "none" }}
        type="text"
        placeholder="Enter the CMDR here (without CMDR)"
        className=" text-xl bg-transparent border-b-2 border-neutral-600 focus:border-orange-400 focus:ring-0 focus:ring-offset-0 focus:ring-transparent"
      />

      {result ? <ResultContainer result={result} /> : <p>No results.</p>}
    </Form>
  );
}

function ResultContainer({ result }: { result: string[] }) {
  return (
    <div className=" grid grid-cols-4 gap-3 mt-4">
      {result.map((e, i) => (
        <Link
          tabIndex={10 + i + 1}
          className="bg-slate-900 p-1 text-xl hover:bg-orange-600 "
          key={e}
          to={"../cmdrs/" + e}
        >
          {e}
        </Link>
      ))}
    </div>
  );
}
