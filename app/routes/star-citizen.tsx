import { Outlet } from "@remix-run/react";
import { Footer } from "~/components/footer";

export default function SCLanding() {
  return (
    <div className="bg-slate-950 text-white w-full max-h-full items-center  min-h-full flex flex-col justify-center flex-1 ">
      <div className="flex justify-between flex-row mt-28 w-full max-w-5xl px-2 ">
        <Outlet />
      </div>
      <div className="flex-1"></div>
      <Footer />
    </div>
  );
}
