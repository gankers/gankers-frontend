/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
// ^^^- issues arise from Backdrop handler
import {
  Link,
  NavLink,
  Outlet,
  isRouteErrorResponse,
  json,
  useLoaderData,
  useNavigation,
  useRouteError,
} from "@remix-run/react";
import { useState } from "react";
import ggiImage from "~/assets/ggi.png";
import { Footer } from "~/components/footer";
import { MdiForwardburger } from "~/components/icons/burgerRight";
import { IcBaselineSearch } from "~/components/icons/search";
import leaderboardServiceServer from "~/services/leaderboard.service.server";

// Mobile has a bit of a modified layout
const NavHomeGroup = ({ mobile }: { mobile?: boolean }) => (
  <Link to={"."}>
    <div className="flex flex-row p-2 gap-2 items-center">
      <img src={ggiImage} className="w-12 h-12" alt="" />
      <div className={`${!mobile && "hidden"} lg:flex flex-col`}>
        <p className="text-3xl font-black">ED Killboard</p>
        <p className="text-xs font-bold -mt-2">Elite: Dangerous PVP Tracker</p>
      </div>
    </div>
  </Link>
);

export const loader = async () => {
  const count = await leaderboardServiceServer.getTotalKillCount();
  return json({ count });
};

export function ErrorBoundary() {
  const __error = useRouteError();
  const error = isRouteErrorResponse(__error) ? __error! : undefined;
  console.error(error);

  if (error?.status === 404) {
    return (
      <div>
        <h1>
          <span className=" text-orange-400 text-9xl font-black">404</span>
        </h1>
        <p className="text-white text-3xl">Route not found</p>
        <Link to={"/elite"}>
          <button className=" bg-slate-400/20 p-2 mt-4">
            Back to main page
          </button>
        </Link>
      </div>
    );
  }

  return (
    <div>
      <h1>
        <span className=" text-orange-400 text-9xl font-black">
          {error?.status ?? ":/"}
        </span>
      </h1>
      <p className="text-white text-3xl">{error?.statusText}</p>
      <Link to={"/elite"}>
        <button className=" bg-slate-400/20 p-2 mt-4">Back to main page</button>
      </Link>
    </div>
  );
}

const navLinksLeft: { name: string; to: string }[] = [
  { name: "Leaderboard", to: "./leaderboard" },
  { name: "Weekly", to: "./weekly" },
  { name: "Recent", to: "./recent" },
  { name: "Powers", to: "./powers" },
  { name: "Submit Kills", to: "./submit" },
];

interface HeaderProps {
  killCount: number | undefined;
}

const Header = ({ killCount }: HeaderProps) => {
  const [mobileNavOut, setMobileNavOut] = useState(false);

  const navigation = useNavigation();

  if (navigation.state === "loading" && mobileNavOut) {
    setMobileNavOut(false);
  }

  return (
    <>
      {/* Desktop Nav */}
      <div id="desktop-nav" className="hidden sm:block fixed top-0 w-full z-50">
        <nav className="flex flex-row items-center   bg-slate-800 w-full">
          <NavHomeGroup />
          {navLinksLeft.map((e) => (
            <NavLink
              className="p-2 py-5 font-black border-b-2 border-transparent text-white  [&.active]:border-orange-500 hover:text-white [&.active]:hover:text-white  hover:bg-white/10 duration-200"
              key={e.to}
              to={e.to}
            >
              {e.name}
            </NavLink>
          ))}
          <div className="flex-1" />
          {killCount && (
            <div className="  mx-3 p-2 rounded-lg text-white">
              <p className="flex flex-col md:flex-row md:gap-2 justify-center items-center">
                <span className=" md:mb-0 -mb-2">Kills:</span>
                <span className=" text-orange-500 font-black">{killCount}</span>
              </p>
            </div>
          )}
          <NavLink
            to="search"
            className=" bg-transparent hover:bg-white/10 p-2 mr-5 rounded-full"
          >
            <IcBaselineSearch className="w-8 h-8" />
          </NavLink>
        </nav>
      </div>
      {/* Mobile Nav */}
      <div
        onClick={() => setMobileNavOut(false)}
        className={` z-50 hidden pointer-events-none [&.nav-active]:block [&.nav-active]:pointer-events-auto fixed w-full h-full bg-transparent backdrop-blur-none duration-500 transition-all [&.nav-active]:backdrop-blur-sm ${mobileNavOut && "nav-active"} `}
      ></div>
      <button
        onClick={() => setMobileNavOut(!mobileNavOut)}
        className={`block z-50 backdrop-blur-sm sm:hidden  duration-200 ease-in-out fixed top-2 left-0 p-3 bg-neutral-900/90 ml-0 [&.nav-active]:ml-[250px] rounded-r-xl group transition-all ${mobileNavOut && "nav-active"}`}
      >
        <MdiForwardburger className="w-8 h-8 group-[.nav-active]:rotate-180 duration-200 ease-in-out transition-all" />
      </button>
      <nav
        className={` z-50 block bg-neutral-900/90 backdrop-blur-sm sm:hidden duration-200 ease-in-out fixed left-0 top-0 w-[250px] h-full ml-[-250px] ${mobileNavOut && "nav-active"} [&.nav-active]:ml-0 group`}
        id="mobile-nav"
      >
        <div className="flex flex-col h-full">
          <NavHomeGroup mobile={true} />
          {[
            { name: "Home", to: "." },
            ...navLinksLeft,
            { name: "Search", to: "search" },
          ].map((e) => (
            <NavLink
              end
              className="p-2 py-5 font-black border-b-2 border-transparent text-white [&.active]:bg-white/10  [&.active]:border-orange-500 hover:text-white [&.active]:hover:text-white  hover:bg-white/10 duration-200"
              key={e.to}
              to={e.to}
            >
              {e.name}
            </NavLink>
          ))}
          <div className="flex-1" />
          <div className="inline-flex flex-row items-center justify-center">
            {killCount && (
              <p className="flex flex-row gap-2 text-2xl p-2 justify-center items-center">
                <span className="">Kills:</span>
                <span className=" text-orange-500 font-black">{killCount}</span>
              </p>
            )}
          </div>
        </div>
      </nav>
    </>
  );
};

export default function EliteLanding() {
  const { count } = useLoaderData<typeof loader>();

  return (
    <div className="bg-slate-950 text-white w-full max-h-full items-center min-h-full flex flex-col justify-start flex-1 ">
      <Header killCount={count} />
      <div
        id="elite_main"
        className="flex items-center justify-between flex-col mt-28 w-full px-2 overflow-x-hidden"
      >
        <Outlet />
      </div>
      <div className="flex-1"></div>
      <Footer />
    </div>
  );
}
