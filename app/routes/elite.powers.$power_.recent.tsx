import { LinksFunction, LoaderFunctionArgs, json } from "@remix-run/node";
import {
  Link,
  useLoaderData,
  useParams,
  useSearchParams,
} from "@remix-run/react";
import { z } from "zod";
import RecentKillsTable from "~/components/recentKillsTable";
import ggiTables from "~/styles/ggi-table.scss?url";
import ggiPagination from "~/styles/ggi-pagination.scss?url";
import powerColours from "~/components/icons/powers/powerColors";
import { PowerIcon } from "~/components/icons/powers/power_icon";
import powersService from "~/services/powers.service.server";
import { useMemo } from "react";
import ConsiderUnpledgeToggle from "~/components/powerplay/unpledgedToggle";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: ggiTables },
  { rel: "stylesheet", href: ggiPagination },
];

export const loader = async ({ request, params }: LoaderFunctionArgs) => {
  const powerName = params["power"];
  const url = new URL(request.url);
  const withUnpledged = url.searchParams.get("withUnpledged") === "true";
  const unsafePage = new URL(request.url).searchParams.get("page") ?? "1";
  const pageParseResult = z
    .number()
    .min(1)
    .int()
    .default(1)
    .safeParse(Number(unsafePage));
  const count = 50;
  const page = pageParseResult.success ? pageParseResult.data : 1;

  const result = await powersService.getRecentPowerKills(
    powerName!,
    1,
    20,
    withUnpledged,
  );
  if (!result) {
    throw new Response("Power not found", { status: 404 });
  }
  return json({ result, hasMore: result.length == count });
};

export default function EliteRecent() {
  const { result, hasMore } = useLoaderData<typeof loader>();
  const [searchParams] = useSearchParams();
  const power = useParams()["power"] ?? "";
  const colour = powerColours[power!];
  const bgColour = colour + "22";
  const page = Number(searchParams.get("page") ?? "1");

  const [searchParam, setSearchParams] = useSearchParams();

  const considerUnpledgedCmdrs = useMemo(() => {
    const entry = searchParam.get("withUnpledged");
    return entry === "true";
  }, [searchParam]);

  const toggleShouldConsiderUnpledgedCmdrs = () => {
    setSearchParams((prev) => {
      if (considerUnpledgedCmdrs) {
        prev.delete("withUnpledged");
      } else {
        prev.set("withUnpledged", "true");
      }
      return prev;
    });
  };

  const PrevBtn = () => <button disabled={page < 2}>Previous Page</button>;

  const NextBtn = () => <button disabled={!hasMore}>Next Page</button>;

  return (
    <div className="flex flex-col w-full items-center gap-3 mb-5 px-12">
      <h1 className="w-full flex flex-row gap-2 text-4xl">
        <span>
          Recent Kills for{" "}
          <Link style={{ color: colour }} to={"/elite/powers/" + power}>
            {power}
          </Link>{" "}
        </span>
        <span>
          <Link className="" to={"/elite/powers/" + power}>
            <PowerIcon
              style={{ color: powerColours[power] }}
              className="w-8 h-8"
              power={power}
            />
          </Link>
        </span>
      </h1>
      <p className="w-full text-neutral-300 -mt-3">
        This board contains recent kills, sorted by when they happened. Only
        showing kills where a <Link to={"/elite/powers/" + power}>{power}</Link>{" "}
        pledge took part.
      </p>
      <section id="filter_settings" className="p-2 w-full">
        <ConsiderUnpledgeToggle
          onChangeCallback={toggleShouldConsiderUnpledgedCmdrs}
          checked={considerUnpledgedCmdrs}
        />
      </section>
      <RecentKillsTable recentData={result} />
      <div className="flex flex-row gap-3 pagination-group">
        {page < 2 ? (
          <PrevBtn />
        ) : (
          <Link to={{ search: "page=" + (page - 1) }}>
            <PrevBtn />
          </Link>
        )}
        <div>{page}</div>
        {!hasMore ? (
          <NextBtn />
        ) : (
          <Link to={{ search: "page=" + (page + 1) }}>
            <NextBtn />
          </Link>
        )}
      </div>
    </div>
  );
}
