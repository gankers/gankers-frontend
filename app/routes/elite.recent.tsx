import { LinksFunction, LoaderFunctionArgs, json } from "@remix-run/node";
import { Link, useLoaderData, useSearchParams } from "@remix-run/react";
import { z } from "zod";
import RecentKillsTable from "~/components/recentKillsTable";
import recentKillsServiceServer from "~/services/recentKills.service.server";
import ggiTables from "~/styles/ggi-table.scss?url";
import ggiPagination from "~/styles/ggi-pagination.scss?url";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: ggiTables },
  { rel: "stylesheet", href: ggiPagination },
];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const unsafePage = new URL(request.url).searchParams.get("page") ?? "1";
  const pageParseResult = z
    .number()
    .min(1)
    .int()
    .default(1)
    .safeParse(Number(unsafePage));
  const count = 50;
  const page = pageParseResult.success ? pageParseResult.data : 1;

  const result = await recentKillsServiceServer.getPage(page, count);
  return json({ result, hasMore: result.length == count });
};

export default function EliteRecent() {
  const { result, hasMore } = useLoaderData<typeof loader>();
  const [searchParams] = useSearchParams();

  const page = Number(searchParams.get("page") ?? "1");

  const PrevBtn = () => <button disabled={page < 2}>Previous Page</button>;

  const NextBtn = () => <button disabled={!hasMore}>Next Page</button>;

  return (
    <div className="flex flex-col w-full items-center gap-3 mb-5">
      <h1 className="w-full text-4xl">Recent Kills</h1>
      <p className="w-full text-neutral-300 -mt-3">
        This board contains recent kills, sorted by when they happened.
      </p>
      <RecentKillsTable recentData={result} />
      <div className="flex flex-row gap-3 pagination-group">
        {page < 2 ? (
          <PrevBtn />
        ) : (
          <Link to={{ search: "page=" + (page - 1) }}>
            <PrevBtn />
          </Link>
        )}
        <div>{page}</div>
        {!hasMore ? (
          <NextBtn />
        ) : (
          <Link to={{ search: "page=" + (page + 1) }}>
            <NextBtn />
          </Link>
        )}
      </div>
    </div>
  );
}
