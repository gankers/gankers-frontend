import { LinksFunction, LoaderFunctionArgs, json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { useEffect, useState } from "react";
import { z } from "zod";
import { OverlayConfigZod } from "~/components/overlay/configurer";
import EventContainer, { EventProps } from "~/components/overlay/event";
import makeEventReceiver from "~/components/overlay/eventReceiver";
import environmentService from "~/services/environment.service.server";

import overlayStyles from "~/styles/overlay.scss?url";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: overlayStyles },
];
export const loader = async ({ params, request }: LoaderFunctionArgs) => {
  const configBase64 = params["config"];

  const url = new URL(request.url);
  const isDemo = url.searchParams.get("demo") !== null;

  if (typeof configBase64 !== "string") {
    throw new Response("Missing Config String? Should never happen", {
      status: 500,
    });
  }
  let config: z.infer<typeof OverlayConfigZod>;
  try {
    const asJsonString = Buffer.from(configBase64, "base64url").toString(
      "utf-8",
    );
    config = OverlayConfigZod.parse(JSON.parse(asJsonString));
  } catch (err) {
    throw new Response(
      "Failed to parse the Configuration from the URL. Are you sure it is correct?",
      { status: 400 },
    );
  }

  return json({
    config,
    sseUrl: isDemo
      ? undefined
      : environmentService.backendPublicHost + "/elite/leaderboard/live-sse",
  });
};

export default function OverlayLanding() {
  const { config, sseUrl } = useLoaderData<typeof loader>();
  const [events, setEvents] = useState<EventProps[]>([]);

  useEffect(() => {
    const { abort } = makeEventReceiver(config, handleEvent, sseUrl);
    function handleEvent(event: EventProps) {
      setEvents((events) => [...events, event]);
      setTimeout(() => {
        setEvents((events) => events.filter((e) => e.uuid !== event.uuid));
      }, config.notificationUpMillis);
    }

    return abort;
  }, [config, sseUrl]);

  return (
    <EventContainer
      additionalClasses={" min-h-[100vh]"}
      events={events}
      config={config}
    ></EventContainer>
  );
}
