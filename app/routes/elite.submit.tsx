import { LinksFunction } from "@remix-run/node";
import submitStyles from "~/styles/elite-submit.scss?url";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: submitStyles },
];

export default function EliteSubmit() {
  return (
    <section>
      <h1 className="text-3xl font-black">
        Instructions for submitting Elite Kills
      </h1>
      <div className="w-8 h-1 my-2 bg-white"></div>
      <section id="">
        <h2>Get ED Market Connector</h2>
        <p>
          EDMC is a Python-based Program you can run on PC. It is used to sync
          data between Elite: Dangerous and 3rd Party Tooling. We use a
          custom-made Plugin for EDMC to send Kills and Deaths to our Backend.
        </p>
        <p>
          You can get EDMC{" "}
          <a
            target="_blank"
            href="https://github.com/EDCD/EDMarketConnector/releases"
            rel="noreferrer"
          >
            from their Github Releases
          </a>
          .
        </p>
      </section>
      <section id="">
        <h2>Get our EDMC Plugin</h2>
        <p>
          We made a plugin that reads the Journal and scans for Kills and
          Deaths. You can download it{" "}
          <a
            target="_blank"
            href="https://gitlab.com/gankers/EDMC-PvPBot/-/releases"
            rel="noreferrer"
          >
            from our Gitlab Releases.
          </a>
        </p>
        <p>
          Install it by dragging the contents of the Release into EDMCapos;s
          Plugin Directory. You can find this Directory by going to Settings
          &gt; Plugins &gt; Plugins folder and clicking on the <b>Open</b>{" "}
          Button.
        </p>
        <p>
          You{"'"}ve done it right when the Plugins-Folder has a Folder (e.g.
          EDMC-PvPBot) which, when opened, contains a <code>load.py</code>.
        </p>
      </section>
      <section id="">
        <h2>Get an API Key</h2>
        <p>
          As of today, Submissions are only available to Members in the{" "}
          <a
            target="_blank"
            href="https://discord.gg/4N2jKQpxEZ"
            rel="noreferrer"
          >
            Galactic Gank Initiative Discord
          </a>
          . To get the Member Role you must provide one Clip of you ganking
          someone. Bonus points for keeping it low-effort and short.
        </p>
        <p>
          Once you{"'"}re in the server and Role{"'"}d up, you can run{" "}
          <code>/pvpregister</code>. Our Discord Bot will send you an API Key.
          Make sure to quickly copy it as the API Key will get hidden after a
          short while.
        </p>
      </section>
      <section id="">
        <h2>Setup the Plugin</h2>
        <p>
          The first time you run EDMC with our Plugin, you will get a little
          onboarding. The Plugin will ask you for CMDRs it should look at,{" "}
          <b>your API Key</b> and check for any Kills in your older Logs.
          Afterwards, you ill get the option to submit these {"'historic'"}{" "}
          kills.{" "}
        </p>
      </section>
      <section id="">
        <h2>Go killing!</h2>
        <p>
          From here on out, simply have EDMC running while you play Elite. On
          startup, you should see a message that the API Key is valid or an
          error if that is not the case.
        </p>
        <p>
          If you lose your API Key, simply run <code>/pvpregister</code> again.
          This will invalidate your old API Key. If you forget to run EDMC and
          miss a kill, you can go to the Settings and activate the{" "}
          {"Historic Aggregate"} Checkbox and Restart EDMC.
        </p>
      </section>
    </section>
  );
}
