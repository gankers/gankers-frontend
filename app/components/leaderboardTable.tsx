import { Link } from "@remix-run/react";
import { LeaderboardResult } from "~/generated/elite_leaderboard";

interface Props {
  leaderboardData: LeaderboardResult["result"];
  offset?: number | undefined;
}

export default function LeaderboardTable({ leaderboardData, offset }: Props) {
  return (
    <table className="ggi-table text-sm md:text-lg">
      <thead>
        <tr className="">
          <th>#</th>
          <th className=" text-left">CMDR</th>
          <th className=" text-right">Kills</th>
          <th className=" text-right">Deaths</th>
        </tr>
      </thead>
      <tbody>
        {leaderboardData.map((e, i) => (
          <tr key={e.cmdr_name}>
            <td className=" pr-2">{i + 1 + (offset ?? 0)}</td>
            <td>
              <Link to={`/elite/cmdrs/${encodeURIComponent(e.cmdr_name)}`}>
                {e.cmdr_name}
              </Link>
            </td>
            <td className="text-right">{e.kills ?? 0}</td>
            <td className="text-right">{e.deaths ?? 0}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
