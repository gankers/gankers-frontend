import { Link } from "@remix-run/react";
import { KillboardEntry } from "~/generated/elite_leaderboard";
import powerColours from "./icons/powers/powerColors";

import hudson from "./icons/powers/zachary-hudson.svg?url";
import { PowerIcon } from "./icons/powers/power_icon";

interface Props {
  recentData: KillboardEntry[];
  selfCmdr?: string;
  spaced?: boolean;
}

function BottomColumn({
  ship,
  cmdr,
}: {
  ship: string | undefined | null;
  cmdr: KillboardEntry["killer" | "victim"];
}) {
  return (
    (ship || cmdr.squadron) && (
      <div className="text-sm text-gray-400 -mt-1 z-10">
        {cmdr.squadron && <span className="text-white">[{cmdr.squadron}]</span>}{" "}
        {ship}
      </div>
    )
  );
}

interface RowCmdrInfoProps {
  isRightSide: boolean;
  cmdrInfo: KillboardEntry["killer"];
}

function RowCmdrInfo({ isRightSide, cmdrInfo }: RowCmdrInfoProps) {
  let borderColour = "transparent";
  let gradient = "transparent";
  const killerShip = cmdrInfo.ship_name ?? cmdrInfo.ship_id;
  if (!!cmdrInfo.power) {
    const colour = powerColours[cmdrInfo.power];
    if (!!colour) {
      borderColour = colour;
      gradient = colour + "55";
    }
  }

  return (
    <td
      style={{
        borderLeftColor: borderColour,
        background: `linear-gradient(${isRightSide ? "" : "-"}90deg, rgba(0,0,0,0) 0%, ${gradient} 100%)`,
      }}
      className={`flex p-0 flex-1 ${isRightSide && "flex-row-reverse"} relative overflow-clip`}
    >
      {cmdrInfo.power ? (
        <Link
          to={"/elite/powers/" + cmdrInfo.power}
          style={{ backgroundColor: borderColour }}
          className="w-2 h-full"
        ></Link>
      ) : (
        <div
          style={{ backgroundColor: borderColour }}
          className="w-2 h-full"
        ></div>
      )}
      {cmdrInfo.power && (
        <PowerIcon
          style={{
            color: gradient,
          }}
          className={`pointer-events-none w-24 h-24 bottom-0 -mb-5  absolute ${isRightSide ? "left-12" : "right-12"}`}
          power={cmdrInfo.power}
        />
      )}
      <Link
        className={`w-full h-full flex-1 flex flex-col ${isRightSide && "items-end"} justify-center p-2`}
        to={`/elite/cmdrs/${cmdrInfo.name}`}
      >
        <div className="z-10 text-lg">{cmdrInfo.name}</div>
        <BottomColumn ship={killerShip} cmdr={cmdrInfo} />
      </Link>
    </td>
  );
}

export default function RecentKillsTable({
  recentData,
  selfCmdr,
  spaced,
}: Props) {
  return (
    <table className={`text-sm md:text-lg ggi-table ${spaced ? "spaced" : ""}`}>
      <thead className="flex flex-col">
        <tr className="flex flex-row w-full ">
          <th className="flex-1 text-left">Killer</th>
          <th className="flex-1 text-center">System</th>
          <th className="flex-1 text-right">Victim</th>
        </tr>
      </thead>
      <tbody className=" flex flex-col">
        {recentData.map((e) => {
          const datetime = new Date(e.kill_timestamp);
          // God… date handling in JS is horrible
          const timestring = datetime
            .toISOString()
            .slice(0, 16)
            .replace("T", " ")
            .split(" ");

          const killType = (() => {
            if (!selfCmdr) {
              return "neutral";
            }
            if (e.killer.name.toLowerCase() === selfCmdr.toLowerCase()) {
              return "good";
            }
            return "bad";
          })();

          return (
            <tr
              className="flex flex-col mb-3 rounded-lg xs:rounded-none overflow-clip xs:mb-0 mt-1  xs:flex-row even:bg-slate-900 odd:bg-slate-900/50 "
              key={e.interaction_index + e.kill_timestamp}
            >
              <RowCmdrInfo isRightSide={false} cmdrInfo={e.killer} />
              <td
                style={
                  killType !== "neutral"
                    ? {
                        background: `radial-gradient(
                          50% 50% at 50% bottom,
                          #${killType === "good" ? "347928" : "AF1740"}AA, transparent
                        )`,
                      }
                    : {}
                }
                className="flex flex-col justify-center items-center md:min-w-48"
              >
                {e.system_name && (
                  <Link to={`/elite/systems/${e.system_name}`}>
                    {e.system_name}
                  </Link>
                )}
                <div className="flex flex-col  md:flex-row items-center gap-0 md:gap-3">
                  <span className="text-sm">{timestring[0]}</span>
                  <span className="text-lg">{timestring[1]}</span>
                </div>
              </td>
              <RowCmdrInfo isRightSide={true} cmdrInfo={e.victim} />
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
