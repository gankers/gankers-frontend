export interface ConsiderUnpledgeToggleProps {
  onChangeCallback: () => void;
  checked: boolean;
}

export default function ConsiderUnpledgeToggle(
  props: ConsiderUnpledgeToggleProps,
) {
  return (
    <label className="inline-flex items-center cursor-pointer">
      <input
        onChange={props.onChangeCallback}
        type="checkbox"
        value=""
        className="sr-only peer"
        checked={props.checked}
      />
      <div className="relative w-11 h-6 bg-gray-800 rounded-full peer peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
      <span
        title="If this is unchecked, only entries where both killer and victim have a Power are considered"
        className="ms-3 text-sm font-medium "
      >
        Also consider Unpledged CMDRs in metrics?
      </span>
    </label>
  );
}
