import { getPowerPortait } from "~/assets/powerPortraits/powerPortraits";
import { PowerIcon } from "../icons/powers/power_icon";
import { DeathIcon, KillIcon, PersonIcon } from "../icons/kill_icons";

import type powersService from "~/services/powers.service.server";

export interface PowerSummaryArgs {
  bgColour: string;
  colour: string;
  power: string;
  powerInfo: Awaited<ReturnType<typeof powersService.getPowerDetailInfo>>;
}

export default function PowerSummary({
  bgColour,
  colour,
  power,
  powerInfo,
}: PowerSummaryArgs) {
  if (powerInfo === null) {
    return <></>;
  }

  return (
    <section
      id="power-summary"
      style={{ background: bgColour, borderColor: colour, color: colour }}
      className="relative flex flex-row gap-2 w-full items-center border-b-2  p-1 overflow-hidden"
    >
      <PowerIcon
        className=" w-96 absolute right-0 rotate-12 opacity-40 hidden xl:block"
        power={power}
      />
      <img
        className="w-32 lg:w-64 z-10 h-32 lg:h-64 m-4 rounded-lg hidden xs:block "
        src={getPowerPortait(power)}
        alt={"Portrait of " + power}
      />
      <div className="flex flex-col">
        <h1 className="text-4xl lg:text-7xl">{power}</h1>
        <div className="flex flex-row gap-2 lg:gap-12">
          {/* Weekly Info */}
          <div style={{ borderColor: colour }} className="flex flex-col">
            <div className="flex gap-4 flex-row text-2xl lg:text-5xl ">
              <div
                title="Kills this week by agents of this power"
                className="flex-col"
              >
                <KillIcon />
                <span>{powerInfo.week.kills}</span>
              </div>
              <div
                title="Deaths this week by agents of this power"
                className="flex-col"
              >
                <DeathIcon />
                <span>{powerInfo.week.deaths}</span>
              </div>
              <div
                title="Agents with kills or deaths for this power"
                className="flex-col"
              >
                <PersonIcon />
                <span>{powerInfo.week.agents ?? "N/A"}</span>
              </div>
            </div>
            <div style={{ background: colour }} className="w-full h-1"></div>
            <p className="text-2xl">Weekly Data</p>
          </div>
          {/* Global Info */}
          <div style={{ borderColor: colour }} className="flex flex-col">
            <div className="flex gap-4 flex-row text-2xl lg:text-5xl ">
              <div
                title="All-time Kills by agents of this power"
                className="flex-col"
              >
                <KillIcon />
                <span>{powerInfo.total.kills}</span>
              </div>
              <div
                title="All-time Deaths by agents of this power"
                className="flex-col"
              >
                <DeathIcon />
                <span>{powerInfo.total.deaths}</span>
              </div>
            </div>
            <div style={{ background: colour }} className="w-full h-1"></div>
            <p className="text-2xl">All Data</p>
          </div>
        </div>
      </div>
    </section>
  );
}
