import { useClickAway } from "@uidotdev/usehooks";
import { useState } from "react";
import { HexAlphaColorPicker, HexColorPicker } from "react-colorful";
import MaterialSymbolsResetShadow from "../icons/reset";

interface Props {
  defaultColour: string;
  currentColour: string;
  updateCurrentColour: (newColour: string) => void;
  heading: string;
  description: string;
  allowAlpha?: boolean | undefined;
}

export default function ConfigurerColorPicker({
  currentColour,
  updateCurrentColour,
  heading,
  description,
  allowAlpha,
  defaultColour,
}: Props) {
  const popover = useClickAway<HTMLDivElement>(() => toggleIsOpen(false));
  const [isOpen, toggleIsOpen] = useState(false);

  const PickerImpl = allowAlpha ? HexAlphaColorPicker : HexColorPicker;

  return (
    <div className="relative">
      <button
        onClick={() => toggleIsOpen(true)}
        style={{ borderColor: currentColour }}
        className=" z-0 flex flex-row gap-2 min-w-80 border-8 p-4 rounded-xl text-left"
      >
        <div className="flex flex-col">
          <span className="text-xl">{heading}</span>
          <span className="text-neutral-400 text-sm">{description}</span>
        </div>
      </button>
      {isOpen && (
        <div
          style={{ top: "calc(100% + 4px)" }}
          ref={popover}
          className="bg-neutral-800 p-4 absolute min-w-48 min-h-48 flex flex-col rounded-lg z-10"
        >
          <PickerImpl
            className="w-full"
            color={currentColour}
            onChange={updateCurrentColour}
          />
          <button
            onClick={() => updateCurrentColour(defaultColour)}
            className="flex flex-row items-center justify-center mt-3 gap-2 hover:bg-white/10 p-1 rounded-lg"
          >
            <MaterialSymbolsResetShadow className="w-8 h-8" />
            Reset to Default
          </button>
        </div>
      )}
    </div>
  );
}
