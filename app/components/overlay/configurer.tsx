import { useEffect, useState } from "react";
import { z } from "zod";
import ConfigurerColorPicker from "./configurerColorPicker";
import MaterialSymbolsResetShadow from "../icons/reset";

export const OverlayConfigZod = z.object({
  orientation: z.enum(["tr", "tl", "bl", "br"]).default("tr"),
  ownCmdrNames: z.array(z.string()).optional(),
  showSquadrons: z.boolean().default(true),
  onlyShowOwnKillsOnly: z.boolean().default(false),
  // yoinked from https://stackoverflow.com/a/63856391/7583353
  ownInteractionHighlightColour: z
    .string()
    .regex(/#[a-f\d]{3}(?:[a-f\d]?|(?:[a-f\d]{3}(?:[a-f\d]{2})?)?)\b/)
    .optional(),
  outlineRegularEventsColour: z
    .string()
    .regex(/#[a-f\d]{3}(?:[a-f\d]?|(?:[a-f\d]{3}(?:[a-f\d]{2})?)?)\b/)
    .default("#4b4b4b"),
  eventsBackgroundTintColour: z
    .string()
    .regex(/#[a-f\d]{3}(?:[a-f\d]?|(?:[a-f\d]{3}(?:[a-f\d]{2})?)?)\b/)
    .default("#00000055"),
  notificationUpMillis: z.number().finite().positive().default(5000),
  greenBackground: z.boolean().default(false),
});

interface Props {
  currentConfig: z.infer<typeof OverlayConfigZod>;
  setCurrentConfig: (newConfig: z.infer<typeof OverlayConfigZod>) => void;
}

export default function OverlayConfigurer({
  currentConfig,
  setCurrentConfig,
}: Props) {
  const [cmdrsText, setCmdrsText] = useState(
    (currentConfig.ownCmdrNames ?? []).join(","),
  );
  const [durationText, setDurationText] = useState(
    currentConfig.notificationUpMillis + "",
  );

  useEffect(() => {
    setDurationText(currentConfig.notificationUpMillis + "");
  }, [currentConfig]);

  return (
    <div className=" flex flex-col gap-5">
      <div className="w-full flex flex-col">
        <h2 className="text-lg">CMDR Name(s)</h2>
        <input
          value={cmdrsText}
          onChange={(ev) => {
            setCmdrsText(ev.target.value);
            setCurrentConfig({
              ...currentConfig,
              ownCmdrNames: ev.target.value
                .split(",")
                .map((e) => e.trim())
                .filter((e) => e.length > 0),
            });
          }}
          className=" bg-neutral-800 p-2 rounded-lg"
          id="cmdr_names"
          type="text"
        ></input>
        <p className=" text-neutral-400">
          This is needed if you want to highlight own interactions. Enter either
          one name, like <span className="text-white">WDX</span> or a
          comma-separated list like{" "}
          <span className="text-white">WDX,Harry Potter</span>. Case wont matter
          (e.g. wdx vs WDX). You&rsquo;ve done it right when you can see your
          CMDR names correctly split in the boxes below.
        </p>
        <div className="flex flex-row flex-wrap gap-1 text-xs">
          {(currentConfig.ownCmdrNames ?? []).map((e, i) => (
            <span key={i} className=" bg-slate-500 p-1 rounded-lg">
              {e}
            </span>
          ))}
        </div>

        <h2 className="text-lg mt-4">Element Alignment</h2>
        <select
          value={currentConfig.orientation}
          className=" bg-neutral-800 p-2 rounded-lg"
          onChange={(ev) =>
            setCurrentConfig({
              ...currentConfig,
              orientation: OverlayConfigZod.shape.orientation.parse(
                ev.target.value,
              ),
            })
          }
        >
          <option value="tl">Top-Left</option>
          <option value="tr">Top-Right</option>
          <option value="bl">Bottom-Left</option>
          <option value="br">Bottom-Right</option>
        </select>
        <p className="text-neutral-400">
          Where should the Kill Events be aligned? Top-Right will for example
          put them into - you guessed it - the top right corned, just how CS:GO
          does it.
        </p>
        <label className="flex gap-2 items-center mt-4">
          <input
            checked={currentConfig.onlyShowOwnKillsOnly}
            onChange={(ev) => {
              setCurrentConfig({
                ...currentConfig,
                onlyShowOwnKillsOnly: ev.target.checked,
              });
            }}
            className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded"
            type="checkbox"
          ></input>
          <div className="text-lg">Only show own Interactions</div>
        </label>
        <p className=" text-neutral-400">
          This will make sure that only Events where you either died or got a
          kill will get put onto the overlay.
        </p>
        <label className="flex gap-2 items-center mt-4">
          <input
            checked={currentConfig.ownInteractionHighlightColour !== undefined}
            onChange={(ev) => {
              const checked = ev.target.checked;
              setCurrentConfig({
                ...currentConfig,
                ownInteractionHighlightColour: checked ? "#ff0000" : undefined,
              });
            }}
            className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded"
            type="checkbox"
          ></input>
          <div className="text-lg">Highlight own Interactions</div>
        </label>
        <p className=" text-neutral-400">
          This will give kills or deaths where you are involved a red outline
          (color can be changed below)
        </p>
        <label className="flex gap-2 items-center mt-4">
          <input
            checked={currentConfig.showSquadrons}
            onChange={(ev) => {
              const checked = ev.target.checked;
              setCurrentConfig({
                ...currentConfig,
                showSquadrons: checked,
              });
            }}
            className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded"
            type="checkbox"
          ></input>
          <div className="text-lg">Show Squadron Tags</div>
        </label>
        <p className=" text-neutral-400">
          This will add Squad Tags (if available) to the shown cards
        </p>
        <label className="flex gap-2 items-center mt-4">
          <input
            checked={currentConfig.greenBackground}
            onChange={(ev) => {
              setCurrentConfig({
                ...currentConfig,
                greenBackground: ev.target.checked,
              });
            }}
            className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded"
            type="checkbox"
          ></input>
          <div className="text-lg">Use Green Background</div>
        </label>
        <p className=" text-neutral-400">
          Can&apos;t get OBS&apos;s Plugin to pick up the background as proper
          alpha? Then check this box and you will get a full-green Background
          instead.
        </p>
        <h2 className="text-lg mt-4">Color Config</h2>
        <p className=" text-neutral-400">
          Click the coloured panels to change the colour.
        </p>
        <div className="flex flex-row flex-wrap mt-1 gap-3">
          {typeof currentConfig.ownInteractionHighlightColour === "string" && (
            <ConfigurerColorPicker
              heading="Outline Colour for Highlight own Interactions"
              description="This changes the outline colour for any events where you took part in"
              defaultColour="#ff0000"
              currentColour={currentConfig.ownInteractionHighlightColour}
              updateCurrentColour={(color) =>
                setCurrentConfig({
                  ...currentConfig,
                  ownInteractionHighlightColour: color,
                })
              }
            />
          )}
          <ConfigurerColorPicker
            heading="Outline Colour for regular Events"
            description="Outline for regular events"
            defaultColour="#4b4b4b"
            currentColour={currentConfig.outlineRegularEventsColour}
            updateCurrentColour={(color) =>
              setCurrentConfig({
                ...currentConfig,
                outlineRegularEventsColour: color,
              })
            }
          />
          <ConfigurerColorPicker
            allowAlpha
            heading="Background fill"
            description="The background tint for all events"
            defaultColour="#4b4b4b4b"
            currentColour={currentConfig.eventsBackgroundTintColour}
            updateCurrentColour={(color) =>
              setCurrentConfig({
                ...currentConfig,
                eventsBackgroundTintColour: color,
              })
            }
          />
        </div>
        <h2 className="text-lg">Duration</h2>
        <div className="flex flex-row w-full gap-1 items-center">
          <input
            value={durationText}
            onChange={(ev) => {
              const input = ev.currentTarget.value;
              setDurationText(input);

              try {
                const newDuration =
                  OverlayConfigZod.shape.notificationUpMillis.parse(
                    z.coerce.number().parse(input),
                  );
                setCurrentConfig({
                  ...currentConfig,
                  notificationUpMillis: newDuration,
                });
              } catch (err) {
                console.info(
                  "Received invalid Config Change for Duration: ",
                  err,
                );
              }
            }}
            className=" bg-neutral-800 p-2 rounded-lg flex-1"
            type="number"
            min={1}
            step={1}
          ></input>
          <button
            onClick={() =>
              setCurrentConfig({
                ...currentConfig,
                notificationUpMillis:
                  OverlayConfigZod.shape.notificationUpMillis.parse(undefined),
              })
            }
            className="flex flex-row items-center justify-center  gap-2 hover:bg-white/10 p-1 rounded-lg"
          >
            <MaterialSymbolsResetShadow className="w-8 h-8" />
            Reset to Default
          </button>
        </div>
        <p className=" text-neutral-400">
          This defines how long a kill should be displayed. Value is to be
          provided in milliseconds and must be positive.
        </p>
      </div>
    </div>
  );
}
