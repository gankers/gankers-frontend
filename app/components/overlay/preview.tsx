import { SVGProps, useState } from "react";
import { z } from "zod";
import { OverlayConfigZod } from "./configurer";

import ExampleImage from "~/assets/landing/example_elite.webp?url";
import ExampleGif from "~/assets/overlay/output.gif?url";
import EventContainer, { EventProps } from "./event";

interface Props {
  config: z.infer<typeof OverlayConfigZod>;
}

export default function OverlayPreview({ config }: Props) {
  const [configuredBackground, setBackground] = useState<
    "white" | "black" | "image" | "video" | "hidden"
  >("white");

  const usedBackground = config.greenBackground
    ? "green"
    : configuredBackground;

  interface ContainerProps {
    variant: typeof usedBackground;
    children: string | JSX.Element | JSX.Element[];
  }

  function Container({ children, variant }: ContainerProps) {
    if (variant === "white")
      return <div className={`bg-white w-full aspect-video `}>{children}</div>;
    if (variant === "black")
      return <div className={`bg-black w-full aspect-video `}>{children}</div>;
    if (variant === "green")
      return <div className={`bg-[#0f0] w-full aspect-video `}>{children}</div>;
    if (variant === "image") {
      return (
        <div
          style={{ backgroundImage: `url(${ExampleImage})` }}
          className={`bg-cover w-full aspect-video `}
        >
          {children}
        </div>
      );
    }
    if (variant === "video") {
      return (
        <div
          style={{ backgroundImage: `url(${ExampleGif})` }}
          className={`bg-cover w-full aspect-video `}
        >
          {children}
        </div>
      );
    }
  }

  const events: (Omit<EventProps, "config"> & { uuid: string })[] = [
    {
      uuid: "test",
      killer: "ExampleKiller",
      victim: "ExampleVictim",
      victimSquadron: "TEST",
    },
    {
      uuid: "test2",
      killer: "ExampleKiller",
      victim: "ExampleVictim",
      killerSquadron: "TEST",
    },
  ];

  if (
    (config.ownCmdrNames ?? []).length > 0 &&
    config.ownInteractionHighlightColour
  ) {
    events.push({
      uuid: "test-self",
      killer: config.ownCmdrNames![0],
      victim: "ExampleVictim",
      victimSquadron: "TEST",
    });
  }

  return (
    <div className="w-full flex flex-col border border-slate-600 z-30 mt-2">
      <div className="flex flex-row bg-slate-600">
        <button
          title="Full white"
          className=" hover:-translate-y-1 duration-100"
          onClick={() => setBackground("white")}
        >
          <div className="w-6 h-6 rounded-sm bg-white text-black">W</div>
        </button>
        <button
          title="Full black"
          className=" hover:-translate-y-1 duration-100"
          onClick={() => setBackground("black")}
        >
          <div className="w-6 h-6 rounded-sm bg-black text-white">B</div>
        </button>
        <button
          title="Image Background"
          className=" hover:-translate-y-1 duration-100"
          onClick={() => setBackground("image")}
        >
          <MaterialSymbolsImagesmodeOutline className="w-6 h-6" />
        </button>
        <button
          title="Image Background"
          className=" hover:-translate-y-1 duration-100"
          onClick={() => setBackground("video")}
        >
          <MaterialSymbolsVideoCameraBackOutlineSharp className="w-6 h-6" />
        </button>
        <button
          title="Image Background"
          className=" hover:-translate-y-1 duration-100"
          onClick={() => setBackground("hidden")}
        >
          <ClarityEyeHideLine className="w-6 h-6" />
        </button>
        <div className="flex-1" />
        <span className="text-md text-slate-400">
          Selected Mode: {configuredBackground}
        </span>
      </div>
      <Container variant={usedBackground}>
        <EventContainer config={config} events={events}></EventContainer>
      </Container>
    </div>
  );
}

function MaterialSymbolsImagesmodeOutline(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
      viewBox="0 0 24 24"
      {...props}
    >
      <path
        fill="currentColor"
        d="M5 21q-.825 0-1.412-.587T3 19V5q0-.825.588-1.412T5 3h14q.825 0 1.413.588T21 5v14q0 .825-.587 1.413T19 21zm0-2h14V5H5zm1-2h12l-3.75-5l-3 4L9 13zm-1 2V5zm3.5-9q.625 0 1.063-.437T10 8.5t-.437-1.062T8.5 7t-1.062.438T7 8.5t.438 1.063T8.5 10"
      ></path>
    </svg>
  );
}

function MaterialSymbolsVideoCameraBackOutlineSharp(
  props: SVGProps<SVGSVGElement>,
) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
      viewBox="0 0 24 24"
      {...props}
    >
      <path
        fill="currentColor"
        d="M2 20V4h16v6.5l4-4v11l-4-4V20zm2-2h12V6H4zm0 0V6zm1-2h10l-3.45-4.5l-2.3 3l-1.55-2z"
      ></path>
    </svg>
  );
}

function ClarityEyeHideLine(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
      viewBox="0 0 36 36"
      {...props}
    >
      <path
        fill="currentColor"
        d="M25.19 20.4a6.78 6.78 0 0 0 .43-2.4a6.86 6.86 0 0 0-6.86-6.86a6.79 6.79 0 0 0-2.37.43L18 13.23a4.78 4.78 0 0 1 .74-.06A4.87 4.87 0 0 1 23.62 18a4.79 4.79 0 0 1-.06.74Z"
        className="clr-i-outline clr-i-outline-path-1"
      ></path>
      <path
        fill="currentColor"
        d="M34.29 17.53c-3.37-6.23-9.28-10-15.82-10a16.82 16.82 0 0 0-5.24.85L14.84 10a14.78 14.78 0 0 1 3.63-.47c5.63 0 10.75 3.14 13.8 8.43a17.75 17.75 0 0 1-4.37 5.1l1.42 1.42a19.93 19.93 0 0 0 5-6l.26-.48Z"
        className="clr-i-outline clr-i-outline-path-2"
      ></path>
      <path
        fill="currentColor"
        d="m4.87 5.78l4.46 4.46a19.52 19.52 0 0 0-6.69 7.29l-.26.47l.26.48c3.37 6.23 9.28 10 15.82 10a16.93 16.93 0 0 0 7.37-1.69l5 5l1.75-1.5l-26-26Zm9.75 9.75l6.65 6.65a4.81 4.81 0 0 1-2.5.72A4.87 4.87 0 0 1 13.9 18a4.81 4.81 0 0 1 .72-2.47m-1.45-1.45a6.85 6.85 0 0 0 9.55 9.55l1.6 1.6a14.91 14.91 0 0 1-5.86 1.2c-5.63 0-10.75-3.14-13.8-8.43a17.29 17.29 0 0 1 6.12-6.3Z"
        className="clr-i-outline clr-i-outline-path-3"
      ></path>
      <path fill="none" d="M0 0h36v36H0z"></path>
    </svg>
  );
}
