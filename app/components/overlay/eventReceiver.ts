/*eslint no-constant-condition: ["error", { "checkLoops": false }]*/
import { z } from "zod";
import { OverlayConfigZod } from "./configurer";
import { EventProps } from "./event";
/**
 * Based on whether or not an event source is provided we make the assumption that a "real" or a fake event source is used.
 * This returns an async threadgenerator which can be await-looped over.
 */
export default function makeEventReceiver(
  config: z.infer<typeof OverlayConfigZod>,
  newEventCallback: (event: EventProps) => void,
  eventSourceUrl?: string | undefined,
) {
  if (!eventSourceUrl) {
    return makeMockEventReceiver(config.ownCmdrNames ?? [], newEventCallback);
  } else {
    return makeSseEventReceiver(eventSourceUrl, newEventCallback);
  }
}

function makeSseEventReceiver(
  eventSourceUrl: string,
  newEventCallback: (event: EventProps) => void,
) {
  const source = new EventSource(eventSourceUrl);

  function denullify(x: string | undefined | null): string | undefined {
    if (x === null) {
      x = undefined;
    }
    return x;
  }

  let eventCounter = 1;

  const SseEventZod = z.object({
    kill_id: z.union([z.number(), z.null()]).optional(),
    system: z.union([z.string(), z.null()]).optional(),
    killer: z.string(),
    killer_rank: z.number().gte(0),
    victim: z.string(),
    victim_rank: z.number().gte(0),
    killer_ship: z.union([z.string(), z.null()]).optional(),
    victim_ship: z.union([z.string(), z.null()]).optional(),
    killer_squadron: z.union([z.string(), z.null()]).optional(),
    victim_squadron: z.union([z.string(), z.null()]).optional(),
    kill_timestamp: z.union([z.string(), z.null()]).optional(),
  });

  source.addEventListener("message", (ev) => {
    try {
      const parsedMessage = SseEventZod.parse(JSON.parse(ev.data));

      const convertedMessage: EventProps = {
        killer: parsedMessage.killer,
        killerSquadron: denullify(parsedMessage.killer_squadron),
        victim: parsedMessage.victim,
        victimSquadron: denullify(parsedMessage.victim_squadron),
        uuid: eventCounter++ + "",
      };

      newEventCallback(convertedMessage);
    } catch (err) {
      console.error("failed to RX event: ", err);
    }
  });

  return {
    abort: source.close,
  };
}

function makeMockEventReceiver(
  ownCmdrNames: string[],
  newEventCallback: (event: EventProps) => void,
) {
  let isAborted = false;
  console.info("Created Mock SSE Receiver");
  function abort() {
    console.info("Mock SSE Receiver was aborted");
    isAborted = true;
  }

  (async () => {
    const faker = await import("@faker-js/faker").then((e) => e.fakerEN);

    const squadTags = ["GONK", "XMPL", "TEST", "ASDF", "T3ST", "4SDF"];

    const sleep = (delay: number) =>
      new Promise((res) => setTimeout(res, delay));
    let counter = 1;

    while (!isAborted) {
      // Do a new event every 2-5s
      const delay = 2000 + 3000 * Math.random();
      const ownCmdrProbability = ownCmdrNames.length > 0 ? 0.2 : 0;
      const ownCmdrName =
        ownCmdrNames[Math.floor(Math.random() * ownCmdrNames.length)];

      const newEvent: EventProps = {
        uuid: counter + "",
        killer: faker.internet.userName(),
        victim: faker.internet.userName(),
        killerSquadron:
          Math.random() > 0.5
            ? undefined
            : squadTags[Math.floor(Math.random() * squadTags.length)],
        victimSquadron:
          Math.random() > 0.5
            ? undefined
            : squadTags[Math.floor(Math.random() * squadTags.length)],
      };

      if (Math.random() < ownCmdrProbability && ownCmdrName) {
        // We do a self-event
        const isSelfKiller = Math.random() < 0.5;
        if (isSelfKiller) {
          newEvent.killer = ownCmdrName;
        } else {
          newEvent.victim = ownCmdrName;
        }
      }
      await sleep(delay);
      if (isAborted) {
        continue;
      }
      newEventCallback(newEvent);

      counter++;
    }
  })();

  return { abort };
}
