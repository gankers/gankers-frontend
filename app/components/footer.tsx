export interface Props {
  discordID?: string;
}

export function Footer(props: Props) {
  const discordID = props.discordID ?? "4N2jKQpxEZ";
  return (
    <footer className="flex justify-center flex-col p-3 items-center gap-3 text-slate-500 bg-slate-900 w-full">
      <div className="flex flex-row gap-3">
        <p>
          Made by <span title="Discord: wdx">CMDR WDX</span> and{" "}
          <span title="Discord: harrypotter">Harry Potter</span>
        </p>
        <p>
          <a
            target="_blank"
            rel="noreferrer"
            href={"https://discord.gg/" + discordID}
          >
            Discord
          </a>
          {" | "}
          <a
            target="_blank"
            rel="noreferrer"
            href="https://gitlab.com/gankers/gankers-frontend/"
          >
            Source
          </a>
        </p>
      </div>
      <br />
      <p>
        Website makes use of assets from Elite Dangerous, with the permission of
        Frontier Developments plc, for non-commercial purposes. It is not
        endorsed by nor reflects the views or opinions of Frontier Developments
      </p>
    </footer>
  );
}
