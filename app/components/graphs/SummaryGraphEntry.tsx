import {
  WeekSummaryRankEntry,
  WeekSummaryShipEntry,
  WeekSummarySystemEntry,
} from "~/generated/elite_graphs";
import * as Plot from "@observablehq/plot";
import { useEffect, useRef } from "react";

export interface ShipProps {
  data: WeekSummaryShipEntry[];
  widthOverride?: number;
}

export interface RankProps {
  data: WeekSummaryRankEntry[];
  widthOverride?: number;
}

export interface SystemProps {
  data: WeekSummarySystemEntry[];
  widthOverride?: number;
}

export function SummaryShipGraph({ data, widthOverride }: ShipProps) {
  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (data.length === 0) {
      return;
    }

    const plot = Plot.plot({
      title: "Total Kills, grouped by Ship",
      y: { label: "", axis: "left", grid: true },
      marginLeft: 120,
      x: {
        label: "Kills / Deaths",
        grid: true,
      },
      marks: [
        Plot.ruleX([]),

        Plot.rectX(data, {
          margin: 3,
          x: "kills",
          y: "ship_name",
          fill: "ship_color",
          sort: { y: "-x" },
        }),
        Plot.rectX(data, {
          margin: 3,
          x: (d) => -d.deaths,
          fillOpacity: 0.8,
          y: "ship_name",
          fill: "ship_color",
        }),
        Plot.tip(
          data,
          Plot.pointerY({
            x: "kills",
            y: "ship_name",
            fill: "black",
          }),
        ),
      ],
      height: 400,
      width: widthOverride ?? 1024,
      inset: 10,
    });

    containerRef.current!.append(plot);
    return () => plot.remove();
  }, [data, widthOverride]);

  return <div ref={containerRef} />;
}

export function SummaryRankGraph({ data, widthOverride }: RankProps) {
  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (data.length === 0) {
      return;
    }

    const plot = Plot.plot({
      title: "Total Kills, grouped by Rank",
      y: { label: "", axis: "left", grid: true },
      marginLeft: 120,
      x: {
        label: "Kills / Deaths",
        grid: true,
      },
      marks: [
        Plot.ruleX([]),

        Plot.rectX(data, {
          margin: 3,
          x: "kills",
          y: "rank_name",
          fill: "rank_color",
          sort: {
            y: "-x",
          },
        }),
        Plot.rectX(data, {
          margin: 3,
          x: (d) => -d.deaths,
          fillOpacity: 0.8,
          y: "rank_name",
          fill: "rank_color",
        }),
        Plot.tip(
          data,
          Plot.pointerY({
            x: "kills",
            y: "rank_name",
            fill: "black",
            title: (data) =>
              `Rank: ${data.rank_name}\nKills: ${data.kills}\nDeaths: ${data.deaths}`,
          }),
        ),
      ],
      height: 400,
      width: widthOverride ?? 1024,
      inset: 10,
    });

    containerRef.current!.append(plot);
    return () => plot.remove();
  }, [data, widthOverride]);

  return <div ref={containerRef} />;
}

export function SummarySystemsGraph({ data, widthOverride }: SystemProps) {
  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (data.length === 0) {
      return;
    }

    const plot = Plot.plot({
      title: "Total Kills, grouped by System",
      color: {
        scheme: "Sinebow",
      },
      y: { label: "", axis: "left", grid: true },
      marginLeft: 160,
      x: {
        label: "Kills",
        grid: true,
      },
      marks: [
        Plot.ruleX([0]),

        Plot.rectX(data, {
          margin: 3,
          x: "kills",
          y: "system_name",
          sort: {
            y: "-x",
          },
        }),
        Plot.tip(
          data,
          Plot.pointerY({
            x: "kills",
            y: "system_name",
            fill: "black",
            title: (data) =>
              `System: ${data.system_name}\nKills: ${data.kills}`,
          }),
        ),
      ],
      height: 400,
      width: widthOverride ?? 1024,
      inset: 10,
    });

    containerRef.current!.append(plot);
    return () => plot.remove();
  }, [data, widthOverride]);

  return <div ref={containerRef} />;
}
