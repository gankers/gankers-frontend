import { TimedKillsEntry } from "~/generated/elite_graphs";
import * as Plot from "@observablehq/plot";
import { useEffect, useRef } from "react";
import { getWeekOfYear } from "./util";

export interface Props {
  data: TimedKillsEntry[];
}

export default function WeeklyGroupedGraph({ data }: Props) {
  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const mappedData = data.map((e) => ({
      ...e,
      weeks: getWeekOfYear(new Date(e.time)),
    }));
    const plot = Plot.plot({
      title: "Total Kills in the last 20 weeks",
      y: { grid: true, label: "Kills" },
      x: { label: "Week", type: "band" },
      marks: [
        Plot.rectY(mappedData, { margin: 3, x: "weeks", y: "kills" }),
        Plot.text(mappedData, {
          x: "weeks", // Align the text with the bars
          y: (d) => d.kills, // Position the text slightly above the bars
          text: (d) => d.kills, // Set the text to display the value
          dy: -10, // Fine-tune the vertical offset (optional)
          textAnchor: "middle", // Center the text horizontally
        }),
      ],
      height: 200,
      width: 1024,
      inset: 10,
    });

    containerRef.current!.append(plot);
    return () => plot.remove();
  }, [data]);

  return <div ref={containerRef} />;
}
