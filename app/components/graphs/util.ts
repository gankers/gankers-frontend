export function getWeekOfYear(date: Date) {
  // Create a new Date object for the first day of the year (January 1st)
  const firstDayOfYear = new Date(date.getFullYear(), 0, 1);

  // Calculate the number of milliseconds between the given date and the first day of the year
  const timeDifference = Number(date) - Number(firstDayOfYear);

  // Convert the time difference from milliseconds to days
  const dayOfYear = Math.floor(timeDifference / (24 * 60 * 60 * 1000)) + 1;

  // Calculate the week number by dividing by 7
  const weekNumber = Math.ceil(dayOfYear / 7);

  return weekNumber;
}
