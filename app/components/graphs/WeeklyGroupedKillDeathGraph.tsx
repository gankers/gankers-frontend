import { TimedKillsDeathsEntry } from "~/generated/elite_graphs";
import * as Plot from "@observablehq/plot";
import { useEffect, useRef } from "react";
import { getWeekOfYear } from "./util";

export interface Props {
  data: TimedKillsDeathsEntry[];
}

export default function WeeklyGroupedKillDeathGraph({ data }: Props) {
  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const hasData = data.some((e) => e.deaths > 0 || e.kills > 0);
    if (!hasData) {
      return; // no data, no need to render graph
    }
    const mappedData = data.map((e) => ({
      ...e,
      weeks: getWeekOfYear(new Date(e.time)),
    }));
    const plot = Plot.plot({
      title: "Summed Kills / Deaths, grouped by Week",
      y: { grid: true, label: "Kills" },
      x: { label: "Week", type: "band" },
      marks: [
        Plot.rectY(mappedData, {
          margin: 3,
          x: "weeks",
          y: "kills",
          fill: "darkgreen",
        }),
        Plot.rectY(
          mappedData.map((e) => ({ ...e, deaths: -e.deaths })),
          {
            margin: 3,
            x: "weeks",
            y: "deaths",
            fill: "darkred",
          },
        ),
        Plot.text(
          mappedData.filter((e) => e.kills > 0),
          {
            x: "weeks", // Align the text with the bars
            y: (d) => d.kills, // Position the text slightly above the bars
            text: (d) => d.kills, // Set the text to display the value
            dy: -10, // Fine-tune the vertical offset (optional)
            dx: -15,
            textAnchor: "start", // Center the text horizontally
          },
        ),
        Plot.text(
          mappedData.filter((e) => e.deaths > 0),
          {
            x: "weeks", // Align the text with the bars
            y: (d) => d.kills, // Position the text slightly above the bars
            text: (d) => d.deaths, // Set the text to display the value
            dy: -10, // Fine-tune the vertical offset (optional)
            dx: 15,
            textAnchor: "end", // Center the text horizontally
            fill: "gray",
          },
        ),
        Plot.ruleY([0]),
      ],
      height: 200,
      width: 1024,
      inset: 10,
    });

    containerRef.current!.append(plot);
    return () => plot.remove();
  }, [data]);

  return <div ref={containerRef} />;
}
