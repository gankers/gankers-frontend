import { useEffect, useRef } from "react";
import {
  TimedRankKillsDeathsEntry,
  TimedShipKillsDeathsEntry,
  TimedSystemKillsEntry,
} from "~/generated/elite_graphs";
import * as Plot from "@observablehq/plot";

interface Props {
  ranks: TimedRankKillsDeathsEntry[];
  systems: TimedSystemKillsEntry[];
  ships: TimedShipKillsDeathsEntry[];
}

export default function TimedGroupedGraph({ ranks, systems, ships }: Props) {
  // This looks at the top 10 ships, groups everything else is ignored

  return (
    <>
      <TimedShipGraph data={ships} />
      <TimedRankGraph data={ranks} />
      <TimedSystemGraph data={systems} />
    </>
  );
}

export function TimedShipGraph({ data }: { data: Props["ships"] }) {
  const containerRef = useRef<HTMLDivElement>(null);

  type EntryWithDate = Omit<Props["ships"][number], "time"> & { time: Date };

  useEffect(() => {
    const converted: EntryWithDate[] = data.map((e) => ({
      ...e,
      time: new Date(e.time),
    }));

    const asMap: Record<string, EntryWithDate[]> = {};

    converted.forEach((e) => {
      if (!(e.ship_name in asMap)) {
        asMap[e.ship_name] = [];
      }
      asMap[e.ship_name].push(e);
    });

    const sets = Object.values(asMap);
    const plot = Plot.plot({
      title: "Top 10 Ships, grouped hourly",
      color: {
        legend: true,
        scheme: "Sinebow",
      },
      y: { grid: true, label: "Kills" },
      x: { label: "Time UTC" },
      marks: [
        Object.values(sets).map((e) => [
          Plot.rectY(e, {
            x: "time",
            fill: "ship_color",
            y: "kills",
            order: "ship_name",
            interval: "1 hours",
          }),
          Plot.rectY(e, {
            x: "time",
            fill: "ship_color",
            y: (d) => -d.deaths,
            opacity: 0.8,
            order: "ship_name",
            interval: "1 hours",
          }),
        ]),
        Plot.ruleY([0]),
      ],
      height: 400,
      width: 1024,
      inset: 10,
    });

    containerRef.current!.append(plot);
    return () => plot.remove();
  }, [data]);

  return <div ref={containerRef} />;
}

export function TimedRankGraph({ data }: { data: Props["ranks"] }) {
  const containerRef = useRef<HTMLDivElement>(null);

  type EntryWithDate = Omit<Props["ranks"][number], "time"> & { time: Date };

  useEffect(() => {
    const converted: EntryWithDate[] = data.map((e) => ({
      ...e,
      time: new Date(e.time),
    }));

    const asMap: Record<string, EntryWithDate[]> = {};

    converted.forEach((e) => {
      if (!(e.rank_name in asMap)) {
        asMap[e.rank_name] = [];
      }
      asMap[e.rank_name].push(e);
    });

    const sets = Object.values(asMap);
    const plot = Plot.plot({
      title: "Ranks grouped hourly",
      color: {
        legend: true,
        scheme: "Sinebow",
      },
      y: { grid: true, label: "Kills" },
      x: { label: "Time UTC" },
      marks: [
        Object.values(sets).map((e) => [
          Plot.rectY(e, {
            x: "time",
            fill: "rank_color",
            y: "kills",
            order: "rank_name",
            z: "rank_name",
            interval: "1 hours",
          }),
          Plot.rectY(e, {
            x: "time",
            fill: "rank_color",
            y: (d) => -d.deaths,
            opacity: 0.6,
            order: "rank_name",
            z: "rank_name",
            interval: "1 hours",
          }),
        ]),
        Plot.ruleY([0]),
      ],
      height: 400,
      width: 1024,
      inset: 10,
    });

    containerRef.current!.append(plot);
    return () => plot.remove();
  }, [data]);

  return <div ref={containerRef} />;
}

export function TimedSystemGraph({ data }: { data: Props["systems"] }) {
  const containerRef = useRef<HTMLDivElement>(null);

  type EntryWithDate = Omit<Props["systems"][number], "time"> & { time: Date };

  useEffect(() => {
    const converted: EntryWithDate[] = data.map((e) => ({
      ...e,
      time: new Date(e.time),
    }));

    const asMap: Record<string, EntryWithDate[]> = {};

    converted.forEach((e) => {
      if (!(e.system_name in asMap)) {
        asMap[e.system_name] = [];
      }
      asMap[e.system_name].push(e);
    });

    const sets = Object.values(asMap);
    const plot = Plot.plot({
      title: "Top 10 Systems, grouped hourly",
      color: {
        legend: true,
        scheme: "Sinebow",
      },
      y: { grid: true, label: "Kills" },
      x: { label: "Time UTC" },
      marks: [
        Object.values(sets).map((e) => [
          Plot.rectY(e, {
            x: "time",
            fill: "system_name",
            y: "kills",
            order: "kills",
            z: "system_name",
            interval: "1 hours",
          }),
        ]),
        Plot.ruleY([0]),
      ],
      height: 400,
      width: 1024,
      inset: 10,
    });

    containerRef.current!.append(plot);
    return () => plot.remove();
  }, [data]);

  return <div ref={containerRef} />;
}
