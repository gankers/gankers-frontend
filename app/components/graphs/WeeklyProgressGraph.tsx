import { TimedCmdrKillsEntry } from "~/generated/elite_graphs";
import * as Plot from "@observablehq/plot";
import { useEffect, useMemo, useRef } from "react";

export interface Props {
  data: TimedCmdrKillsEntry[];
}

export default function WeeklyProgressGraph({ data }: Props) {
  const containerRef = useRef<HTMLDivElement>(null);

  type EntryWithDate = Omit<TimedCmdrKillsEntry, "time"> & { time: Date };

  const flattened = useMemo(() => {
    const flattened: Record<string, EntryWithDate[]> = {};

    for (const entry of data) {
      if (flattened[entry.cmdr] === undefined) {
        flattened[entry.cmdr] = [];
      }
      flattened[entry.cmdr].push({ ...entry, time: new Date(entry.time) });
    }
    return flattened;
  }, [data]);

  useEffect(() => {
    const plot = Plot.plot({
      title: "Top 10 CMDRs' kills this week, grouped hourly.",
      color: {
        legend: true,
        scheme: "Sinebow",
      },
      y: { grid: true, label: "Kills" },
      x: { label: "Time UTC" },
      marks: Object.values(flattened).map((e) =>
        Plot.rectY(e, {
          x: "time",
          fill: "cmdr",
          y: "kills",
          order: "cmdr",
          z: "cmdr",
          interval: "1 hours",
        }),
      ),
      height: 200,
      width: 1024,
      inset: 10,
    });

    containerRef.current!.append(plot);
    return () => plot.remove();
  }, [flattened]);

  return <div ref={containerRef} />;
}
