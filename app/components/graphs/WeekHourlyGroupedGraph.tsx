import { TimedKillsEntry } from "~/generated/elite_graphs";
import * as Plot from "@observablehq/plot";
import { useEffect, useRef } from "react";
import { getWeekOfYear } from "./util";

export interface Props {
  data: TimedKillsEntry[];
}

export default function WeekHourlyGroupedGraph({ data }: Props) {
  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (data.length === 0) {
      return;
    }

    const mappedData = data.map((e) => ({ ...e, time: new Date(e.time) }));
    const week = getWeekOfYear(mappedData[0].time);
    const plot = Plot.plot({
      title: "Total Kills for the " + week + ". week",
      y: { grid: true, label: "Kills" },
      x: {
        label: "Hours since start",
        type: "band",
        ticks: "6 hour",
      },
      marks: [
        Plot.rectY(mappedData, {
          margin: 3,
          x: "time",
          y: "kills",
          //title: (d) => `${d.kills} kills at ${d.time.getUTCHours()}:00 UTC `,
        }),
        Plot.tip(
          mappedData,
          Plot.pointerX({ x: "time", y: "kills", fill: "black" }),
        ),
      ],
      height: 200,
      width: 1024,
      inset: 10,
    });

    containerRef.current!.append(plot);
    return () => plot.remove();
  }, [data]);

  return <div ref={containerRef} />;
}
