import { WeekSummary } from "~/generated/elite_graphs";
import {
  SummaryShipGraph,
  SummaryRankGraph,
  SummarySystemsGraph,
} from "./SummaryGraphEntry";

export default function SummaryGroupedGraph({
  ships,
  ranks,
  systems,
}: WeekSummary) {
  return (
    <>
      <SummaryShipGraph data={ships} />
      <SummaryRankGraph data={ranks} />
      <SummarySystemsGraph data={systems} />
    </>
  );
}
