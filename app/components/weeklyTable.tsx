import { Link } from "@remix-run/react";
import { WeeklyLeaderboardEntry } from "~/generated/elite_leaderboard";

interface Props {
  weeklyData: WeeklyLeaderboardEntry[];
  offset?: number;
}

export default function WeeklyLeaderboardTable({
  weeklyData,
  offset = 0,
}: Props) {
  if (weeklyData.length == 0) {
    return <p className="text-xl p-2">No kills in the current period</p>;
  }

  return (
    <table className="ggi-table text-sm md:text-lg">
      <thead>
        <tr className="">
          <th>#</th>
          <th>CMDR</th>
          <th className="align-right">Kills</th>
        </tr>
      </thead>
      <tbody>
        {weeklyData.map((e, i) => (
          <tr key={e.cmdr_name}>
            <td>{i + 1 + offset}</td>
            <td>
              <Link to={`/elite/cmdrs/${encodeURIComponent(e.cmdr_name)}`}>
                {e.cmdr_name}
              </Link>
            </td>
            <td className="text-right">{e.kills ?? 0}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
