import { Link, useLoaderData } from "@remix-run/react";
import LeaderboardTable from "../leaderboardTable";
import shipGoBoom from "~/assets/shipGoBoom.gif?url";
import { loader } from "~/routes/elite.leaderboard";

export function _EliteRecent({
  result,
  hasMore,
  page,
  hideEasterEgg,
}: ReturnType<typeof useLoaderData<typeof loader>> & {
  page: number;
  hideEasterEgg?: boolean;
}) {
  const PrevBtn = () => <button disabled={page < 2}>Previous Page</button>;
  const NextBtn = () => <button disabled={!hasMore}>Next Page</button>;
  hideEasterEgg ??= false;

  return (
    <div className="flex flex-col w-full max-w-xl items-center gap-3 mb-5">
      <LeaderboardTable offset={(page - 1) * 50} leaderboardData={result} />
      <div className="flex flex-row gap-3 pagination-group">
        {page < 2 ? (
          <PrevBtn />
        ) : (
          <Link to={{ search: "page=" + (page - 1) }}>
            <PrevBtn />
          </Link>
        )}
        <div>{page}</div>
        {!hasMore ? (
          <NextBtn />
        ) : (
          <Link to={{ search: "page=" + (page + 1) }}>
            <NextBtn />
          </Link>
        )}
      </div>
      {!hideEasterEgg && (
        <img
          src={shipGoBoom}
          className="w-32 h-32"
          loading="lazy"
          alt="A GIF of a Sidewinder exploding with the text 'Haha Ship go Boom'"
        />
      )}
    </div>
  );
}
