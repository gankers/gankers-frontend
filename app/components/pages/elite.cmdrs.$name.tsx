import { Link, type useLoaderData } from "@remix-run/react";
import type { loader } from "~/routes/elite.cmdrs.$name";
import RecentKillsTable from "../recentKillsTable";
import WeeklyGroupedKillDeathGraph from "../graphs/WeeklyGroupedKillDeathGraph";
import { DeathIcon, KillIcon } from "../icons/kill_icons";
import { PowerIcon } from "../icons/powers/power_icon";
import powerColours from "../icons/powers/powerColors";

export default function _CmdrDetails({
  page,
  hasMore,
  result,
  graphHistory,
}: ReturnType<typeof useLoaderData<typeof loader>> & { hasMore: boolean }) {
  const PrevBtn = () => <button disabled={page < 2}>Previous Page</button>;
  const NextBtn = () => <button disabled={!hasMore}>Next Page</button>;

  return (
    <div className="flex flex-col w-full max-w-screen-lg  gap-3 mb-5">
      <div className="w-full flex flex-col p-4 ">
        <WeeklyGroupedKillDeathGraph data={graphHistory} />
      </div>
      <div className="border-b-2 p-2 flex flex-col sm:flex-row border-orange-400">
        <h1 className="text-3xl inline-flex flex-row items-center gap-2">
          {result.squadron && (
            <span className=" text-neutral-100 text-lg">
              [{result.squadron}]
            </span>
          )}
          <span className="">{result.cmdr_name}</span>
          {result.power && (
            <Link to={"/elite/powers/" + result.power}>
              <PowerIcon
                style={{ color: powerColours[result.power] }}
                className="w-8 h-8"
                power={result.power}
              />
            </Link>
          )}
        </h1>
        <div className="flex-1" />
        <span className="text-3xl flex gap-4">
          <span className="flex flex-row">
            {result.kills} × <KillIcon />
          </span>
          <span className="flex flex-row">
            {result.deaths} × <DeathIcon />
          </span>
        </span>
      </div>

      <RecentKillsTable
        spaced
        recentData={result.page_data}
        selfCmdr={result.cmdr_name}
      />

      {!(page === 1 && !hasMore) && (
        <div className="flex flex-row w-full justify-center gap-3 pagination-group">
          {page < 2 ? (
            <PrevBtn />
          ) : (
            <Link to={{ search: "page=" + (page - 1) }}>
              <PrevBtn />
            </Link>
          )}
          <div>{page}</div>
          {!hasMore ? (
            <NextBtn />
          ) : (
            <Link to={{ search: "page=" + (page + 1) }}>
              <NextBtn />
            </Link>
          )}
        </div>
      )}
    </div>
  );
}
