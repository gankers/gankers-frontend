This folder contains the default exports from Pages (=> Route Components) without the Hooks.
They are extracted because Storybook imports the entire Route-File, which will also depend on Backend-Specific stuff, which breaks the Rendering.

Storybook then imports the components from here, circumventing all the Backend-Specific Imports