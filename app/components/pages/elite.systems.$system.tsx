import { Link, type useLoaderData } from "@remix-run/react";
import type { loader } from "~/routes/elite.systems.$system";
import RecentKillsTable from "../recentKillsTable";

export default function _SystemDetails({
  page,
  hasMore,
  result,
}: ReturnType<typeof useLoaderData<typeof loader>> & { hasMore: boolean }) {
  const PrevBtn = () => <button disabled={page < 2}>Previous Page</button>;
  const NextBtn = () => <button disabled={!hasMore}>Next Page</button>;

  return (
    <div className="flex flex-col w-full  gap-3 mb-5">
      <div className="border-b-2 p-2 flex flex-row border-orange-400">
        <h1 className="text-3xl inline-flex flex-row items-center gap-2">
          <span className="">{result.meta.system_name}</span>
        </h1>
        <div className="flex-1" />
        <span className="text-3xl flex gap-8">
          <span>{result.meta.kill_count} × 🗡️</span>
        </span>
      </div>

      <RecentKillsTable spaced recentData={result.page_data} />

      {!(page === 1 && !hasMore) && (
        <div className="flex flex-row w-full justify-center gap-3 pagination-group">
          {page < 2 ? (
            <PrevBtn />
          ) : (
            <Link to={{ search: "page=" + (page - 1) }}>
              <PrevBtn />
            </Link>
          )}
          <div>{page}</div>
          {!hasMore ? (
            <NextBtn />
          ) : (
            <Link to={{ search: "page=" + (page + 1) }}>
              <NextBtn />
            </Link>
          )}
        </div>
      )}
    </div>
  );
}
