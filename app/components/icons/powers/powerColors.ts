const powerColours: Record<string, string> = {
  "A. Lavigny-Duval": "#8c00fb",
  "Li Yong-Rui": "#00fe62",
  "Aisling Duval": "#00a3fc",
  "Yuri Grom": "#ff4e00",
  "Nakato Kaine": "#8aff00",
  "Felicia Winters": "#ff8c00",
  "Denton Patreus": "#11c9ca",
  "Pranav Antal": "#fff400",
  "Edmund Mahon": "#008306",
  "Archon Delaine": "#d41213",
  "Zemina Torval": "#004dfe",
  "Jerome Archer": "#ff00fe",
  "Zachary Hudson": "#ff0000",
};
export default powerColours;
