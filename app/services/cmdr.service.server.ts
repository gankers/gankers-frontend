import { URLSearchParams } from "url";
import environmentService, {
  type EnvironmentServiceType,
} from "./environment.service.server";
import { CmdrQueryResultZod } from "~/generated/elite_cmdr";
import { z } from "zod";

class CommanderService {
  constructor(private environmentService: EnvironmentServiceType) {}

  public async getCmdrPage(cmdrName: string, page: number) {
    if (page < 1) {
      throw new Error("Page must be positive and greater than 0");
    }
    page = Math.floor(page); // just in case

    const query = new URLSearchParams({
      page: String(page),
    });
    const result = await fetch(
      `${
        this.environmentService.backendhost
      }/elite/cmdrs/${cmdrName}?${query.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    if (result.status == 404) {
      throw new Response("We don't have any info on CMDR " + cmdrName, {
        status: 404,
      });
    }

    if (!result.ok) {
      throw new Error(
        `Failed to get Leaderboard for Page ${page}. Server responsed with Status ${result.status} and Content ${result.text}`,
      );
    }

    const json = await result.json();

    return CmdrQueryResultZod.parse(json);
  }

  public async searchCmdrsByName(searchString: string) {
    if (searchString.length < 1) {
      return [];
    }
    const query = new URLSearchParams({ q: searchString });

    const result = await fetch(
      `${
        this.environmentService.backendhost
      }/elite/search/cmdrs?${query.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    if (!result.ok) {
      throw new Response("Backend responded with non-OK unexpectedly", {
        status: 500,
      });
    }

    return z.array(z.string()).parse(await result.json());
  }
}

export type CommanderServiceType = CommanderService;
export default new CommanderService(environmentService);
