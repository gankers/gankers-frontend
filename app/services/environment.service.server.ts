const authToken = process.env["FRONTEND_BACKEND_JWT"];
const backendHost = process.env["BACKEND_HOST"];
const backendPublicHost = process.env["BACKEND_PUBLIC_HOST"];
const frontendPublicHost = process.env["FRONTEND_PUBLIC_HOST"];

if (typeof authToken !== "string") {
  console.error("Missing ENV VAR FRONTEND_BACKEND_JWT");
  process.exit(1);
}

if (typeof backendHost !== "string") {
  console.error("Missing ENV VAR BACKEND_HOST");
  process.exit(1);
}

if (typeof backendPublicHost !== "string") {
  console.error("Missing ENV VAR BACKEND_PUBLIC_HOST");
  process.exit(1);
}
if (typeof frontendPublicHost !== "string") {
  console.error("Missing ENV VAR FRONTEND_PUBLIC_HOST");
  process.exit(1);
}

class EnvironmentService {
  #authToken: string;
  #backendHost: string;
  #backendPublicHost: string;
  #frontendPublicHost: string;

  get authToken() {
    return this.#authToken;
  }

  get backendhost() {
    return this.#backendHost;
  }

  get backendPublicHost() {
    return this.#backendPublicHost;
  }

  get frontendPublicHost() {
    return this.#frontendPublicHost;
  }

  constructor(
    authToken: string,
    backendHost: string,
    backendPublicHost: string,
    frontendPublicHost: string,
  ) {
    this.#authToken = authToken;
    this.#backendHost = backendHost;
    this.#backendPublicHost = backendPublicHost;
    this.#frontendPublicHost = frontendPublicHost;
  }
}

export type EnvironmentServiceType = EnvironmentService;
export default new EnvironmentService(
  authToken,
  backendHost,
  backendPublicHost,
  frontendPublicHost,
);
