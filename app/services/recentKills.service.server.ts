import { KillboardEntryZod } from "~/generated/elite_leaderboard";
import environmentService, {
  type EnvironmentServiceType,
} from "./environment.service.server";
import { z } from "zod";

class RecentKillsService {
  constructor(private environmentService: EnvironmentServiceType) {}

  public async getPage(page: number, size = 50) {
    if (page < 1) {
      throw new Error("Page must be positive and greater than 0");
    }
    page = Math.floor(page); // just in case

    const query = new URLSearchParams({
      page: String(page),
      size: String(size),
    });
    const result = await fetch(
      `${
        this.environmentService.backendhost
      }/elite/leaderboard/recent?${query.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    if (!result.ok) {
      throw new Error(
        `Failed to get Leaderboard for Page ${page}. Server responsed with Status ${result.status}`,
      );
    }

    const json = await result.json();

    return z.array(KillboardEntryZod).parse(json);
  }
}

export type LeaderboardServiceType = RecentKillsService;
export default new RecentKillsService(environmentService);
