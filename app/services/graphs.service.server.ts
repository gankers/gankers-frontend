import { z } from "zod";
import environmentService, {
  type EnvironmentServiceType,
} from "./environment.service.server";
import {
  TimedCmdrKillsEntryZod,
  TimedKillsDeathsEntryZod,
  TimedKillsEntryZod,
  TimedRankKillsDeathsEntryZod,
  TimedShipKillsDeathsEntryZod,
  TimedSystemKillsEntryZod,
  WeekSummaryZod,
} from "~/generated/elite_graphs";

class GraphsService {
  constructor(private environmentService: EnvironmentServiceType) {}

  public async getWeeklyGrouped() {
    const result = await fetch(
      `${this.environmentService.backendhost}/elite/graphs/weekly-grouped`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );
    if (!result.ok) {
      throw new Error(`Failed to get Weekly Grouped Data`);
    }

    const json = await result.json();

    return z.array(TimedKillsEntryZod).parse(json);
  }

  public async getWeeklyProgress(ts?: string | undefined) {
    const search = new URLSearchParams({});
    if (ts) {
      search.append("ts", ts);
    }

    const result = await fetch(
      `${this.environmentService.backendhost}/elite/graphs/weekly-board`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );
    if (!result.ok) {
      throw new Error(`Failed to get Weekly Grouped Data`);
    }

    const json = await result.json();

    return z.array(TimedCmdrKillsEntryZod).parse(json);
  }

  public async getWeeklySeriesForCMDR(cmdr: string) {
    const result = await fetch(
      `${this.environmentService.backendhost}/elite/graphs/weekly-kd-grouped/${cmdr}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );
    if (!result.ok) {
      throw new Error(`Failed to get Weekly Grouped Data`);
    }

    const json = await result.json();

    return z.array(TimedKillsDeathsEntryZod).parse(json);
  }

  public async getWeekHourlySeries(ts?: string | undefined) {
    const search = new URLSearchParams({});
    if (ts) {
      search.append("ts", ts);
    }

    const result = await fetch(
      `${this.environmentService.backendhost}/elite/graphs/week-hourly-grouped?${search.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    return z.array(TimedKillsEntryZod).parse(await result.json());
  }

  public async getWeekHourlySeriesSystems(ts?: string | undefined) {
    const search = new URLSearchParams({});
    if (ts) {
      search.append("ts", ts);
    }

    const result = await fetch(
      `${this.environmentService.backendhost}/elite/graphs/week-hourly-systems?${search.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    return z.array(TimedSystemKillsEntryZod).parse(await result.json());
  }

  public async getWeekHourlySeriesRanks(ts?: string | undefined) {
    const search = new URLSearchParams({});
    if (ts) {
      search.append("ts", ts);
    }

    const result = await fetch(
      `${this.environmentService.backendhost}/elite/graphs/week-hourly-ranks?${search.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    return z.array(TimedRankKillsDeathsEntryZod).parse(await result.json());
  }

  public async getWeekHourlySeriesShips(ts?: string | undefined) {
    const search = new URLSearchParams({});
    if (ts) {
      search.append("ts", ts);
    }

    const result = await fetch(
      `${this.environmentService.backendhost}/elite/graphs/week-hourly-ships?${search.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    return z.array(TimedShipKillsDeathsEntryZod).parse(await result.json());
  }

  public async getWeekSummary(ts?: string | undefined) {
    const search = new URLSearchParams({});
    if (ts) {
      search.append("ts", ts);
    }

    const result = await fetch(
      `${this.environmentService.backendhost}/elite/graphs/week-summary?${search.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    return WeekSummaryZod.parse(await result.json());
  }
}

export type GraphsServiceType = GraphsService;
export default new GraphsService(environmentService);
