import environmentService, {
  type EnvironmentServiceType,
} from "./environment.service.server";
import {
  PowerAgentBoardEntryZod,
  PowerWeeklyAndTotalGroupZod,
} from "~/generated/elite_powers";
import { z } from "zod";
import { KillboardEntryZod } from "~/generated/elite_leaderboard";

class PowerService {
  constructor(private environmentService: EnvironmentServiceType) {}

  public async getPowersPageOverviewData(withUnpledged: boolean) {
    const result = await fetch(
      `${this.environmentService.backendhost}/elite/powers/weekly-summary?with_unpledged=${withUnpledged}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    return z.array(PowerWeeklyAndTotalGroupZod).parse(await result.json());
  }

  public async getPowerDetailInfo(name: string, withUnpledged: boolean) {
    const result = await fetch(
      `${this.environmentService.backendhost}/elite/powers/weekly-summary/${name}?with_unpledged=${withUnpledged}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );
    if (result.status === 404) {
      return null;
    }

    return PowerWeeklyAndTotalGroupZod.parse(await result.json());
  }

  public async getAgentLeaderboard(
    power: string,
    page: number = 1,
    pageSize: number = 20,
    withUnpledged: boolean,
  ) {
    const result = await fetch(
      `${this.environmentService.backendhost}/elite/powers/weekly-summary/${power}/agents?page=${page}&page_size=${pageSize}&with_unpledged=${withUnpledged}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );
    if (result.status === 404) {
      return null;
    }

    return z.array(PowerAgentBoardEntryZod).parse(await result.json());
  }

  public async getRecentPowerKills(
    power: string,
    page: number,
    pageSize: number,
    withUnpledged: boolean,
  ) {
    const result = await fetch(
      `${this.environmentService.backendhost}/elite/powers/${power}/recent?page=${page}&page_size=${pageSize}&with_unpledged=${withUnpledged}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );
    if (result.status === 404) {
      return null;
    }

    return z.array(KillboardEntryZod).parse(await result.json());
  }
}

export type PowerServiceType = PowerService;
export default new PowerService(environmentService);
