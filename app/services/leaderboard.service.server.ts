import {
  KillCountResponseZod,
  LeaderboardResultZod,
  WeeklyLeaderboardEntryZod,
} from "~/generated/elite_leaderboard";
import environmentService, {
  type EnvironmentServiceType,
} from "./environment.service.server";
import { z } from "zod";

class LeaderboardService {
  constructor(private environmentService: EnvironmentServiceType) {}

  public async getLeaderboardPage(page: number) {
    if (page < 1) {
      throw new Error("Page must be positive and greater than 0");
    }
    page = Math.floor(page); // just in case

    const query = new URLSearchParams({
      page: String(page),
    });
    const result = await fetch(
      `${
        this.environmentService.backendhost
      }/elite/leaderboard?${query.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    if (!result.ok) {
      throw new Error(
        `Failed to get Leaderboard for Page ${page}. Server responsed with Status ${result.status} and Content ${result.text}`,
      );
    }

    const json = await result.json();

    return LeaderboardResultZod.parse(json);
  }

  public async getWeeklyLeaderboardPage(page: number, count: number) {
    if (page < 1) {
      throw new Error("Page must be positive and greater than 0");
    }
    page = Math.floor(page); // just in case

    const query = new URLSearchParams({
      page: String(page),
      size: String(count),
    });
    const result = await fetch(
      `${
        this.environmentService.backendhost
      }/elite/leaderboard/weekly?${query.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    if (!result.ok) {
      throw new Error(
        `Failed to get Leaderboard for Page ${page}. Server responsed with Status ${result.status} and Content ${result.text}`,
      );
    }

    const json = await result.json();

    return z.array(WeeklyLeaderboardEntryZod).parse(json);
  }

  public async getTotalKillCount() {
    const result = await fetch(
      `${this.environmentService.backendhost}/elite/leaderboard/kill-count`,
    );
    if (!result.ok) {
      console.error("failed to get total kill count");
      return undefined;
    }
    return KillCountResponseZod.parse(await result.json())["total_count"];
  }
}

export type LeaderboardServiceType = LeaderboardService;
export default new LeaderboardService(environmentService);
