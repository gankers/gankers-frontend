import { URLSearchParams } from "url";
import environmentService, {
  type EnvironmentServiceType,
} from "./environment.service.server";
import { SystemDataResponseZod } from "~/generated/elite_system";

class SystemService {
  constructor(private environmentService: EnvironmentServiceType) {}

  public async getSystemPage(system: string, page: number) {
    if (page < 1) {
      throw new Error("Page must be positive and greater than 0");
    }
    page = Math.floor(page); // just in case

    const query = new URLSearchParams({
      page: String(page),
    });
    const result = await fetch(
      `${
        this.environmentService.backendhost
      }/elite/systems/${system}?${query.toString()}`,
      {
        headers: {
          authorization: `Bearer ${this.environmentService.authToken}`,
        },
      },
    );

    if (result.status == 404) {
      throw new Response("We don't have any info on System " + system, {
        status: 404,
      });
    }

    if (!result.ok) {
      throw new Error(
        `Failed to get Page ${page}. Server responsed with Status ${result.status} and Content ${result.text}`,
      );
    }

    const json = await result.json();
    return SystemDataResponseZod.parse(json);
  }
}

export type SystemServiceType = SystemService;
export default new SystemService(environmentService);
