import aislingPortrait from "~/assets/powerPortraits/aisling.jpg";
import aldPortrait from "~/assets/powerPortraits/ald.jpg";
import antalPortrait from "~/assets/powerPortraits/antal.jpg";
import delainePortrait from "~/assets/powerPortraits/delaine.jpg";
import dentonPortrait from "~/assets/powerPortraits/denton.jpg";
import gromPortrait from "~/assets/powerPortraits/grom.jpg";
import jeromePortrait from "~/assets/powerPortraits/jerome.jpg";
import kainePortrait from "~/assets/powerPortraits/kaine.jpg";
import lyrPortrait from "~/assets/powerPortraits/lyr.jpg";
import mahonPortrait from "~/assets/powerPortraits/mahon.jpg";
import torvalPortrait from "~/assets/powerPortraits/torval.jpg";
import wintersPortrait from "~/assets/powerPortraits/winters.jpeg";

export function getPowerPortait(power: string) {
  const data: Record<string, string> = {
    "A. Lavigny-Duval": aldPortrait,
    "Li Yong-Rui": lyrPortrait,
    "Aisling Duval": aislingPortrait,
    "Yuri Grom": gromPortrait,
    "Nakato Kaine": kainePortrait,
    "Felicia Winters": wintersPortrait,
    "Denton Patreus": dentonPortrait,
    "Pranav Antal": antalPortrait,
    "Edmund Mahon": mahonPortrait,
    "Archon Delaine": delainePortrait,
    "Zemina Torval": torvalPortrait,
    "Jerome Archer": jeromePortrait,
  };
  return data[power] ?? "";
}
