// Invoke this using vite-node!
// Expects to be run from Gitlab CI Context and from project root!
// npx vite-node tools/make-visreg-diff-comment.ts

import { existsSync, mkdirSync, readFileSync, rmSync } from "fs";
import { basename, join } from "path";
import { globSync } from "glob";
import { $ } from "zx";

const missingVars = [
  "CI_PROJECT_URL",
  "CI_MERGE_REQUEST_PROJECT_ID",
  "CI_MERGE_REQUEST_IID",
  "CI_COMMENT_KEY",
  "CI_JOB_ID",
].filter((e) => typeof process.env[e] === "undefined");

if (missingVars.length > 0) {
  throw new Error("missing env vars: " + missingVars.join(", "));
}

const lostPixelJobID = readFileSync(".lostpixel/id", { encoding: "utf-8" })
  .replaceAll("\n", "")
  .trim();
const lostPixelBase = [
  process.env["CI_PROJECT_URL"]!,
  "-",
  "jobs",
  lostPixelJobID,
  "artifacts",
  "raw",
].join("/"); // Cant use path.join here as it doesnt like the https protocol and turns https:// into https:/

const animationBase = [
  process.env["CI_PROJECT_URL"]!,
  "-",
  "jobs",
  process.env["CI_JOB_ID"]!,
  "artifacts",
  "raw",
].join("/"); // Cant use path.join here as it doesnt like the https protocol and turns https:// into https:/

// https://gitlab.com/gankers/gankers-frontend/-/jobs/7608137563/artifacts/raw/.lostpixel/difference/footer--default.png

console.error("Artifacts Base: " + lostPixelBase);

const differenceData = globSync(".lostpixel/difference/*.png", {
  absolute: true,
}).map((e) => ({
  name: basename(e, ".png"),
  diff: e,
  want: join(e, "..", "..", "baseline", basename(e)),
  have: join(e, "..", "..", "current", basename(e)),
  apng: join(e, "..", "..", "..", ".lostpixel-apng", basename(e)),
}));

// Regenerate APNG Animations for all diffs
rmSync(".lostpixel-apng", { force: true, recursive: true });
mkdirSync(".lostpixel-apng");

await Promise.all(
  differenceData.map(async (e) => {
    const result =
      await $`magick -delay 100 -loop 0 ${e.want} ${e.have} ${e.diff} APNG:${e.apng}`;
    console.log(">> " + result.stdout);
    console.log(`File ${e.apng} exists? ${existsSync(e.apng) ? "yes" : "no"}`);
  }),
);

const differenceLines: string[] = differenceData.flatMap((e) => [
  `## ${e.name}  `,
  "",
  `![](${animationBase}/.lostpixel-apng/${e.name}.png)  `,
  "| Want | Have | Diff |",
  "|------|------|------|",
  `|![Want](${lostPixelBase}/.lostpixel/baseline/${e.name}.png) | ![Have](${lostPixelBase}/.lostpixel/current/${e.name}.png) | ![Diff](${lostPixelBase}/.lostpixel/difference/${e.name}.png)|`,
  "",
]);

const entireMessage: string[] = [
  `# ${differenceData.length} Differences found!`,
  "",
  ...differenceLines,
  "",
  "*This comment was generated automatically via the CI Pipeline*",
];

console.log(entireMessage.join("\n"));

// Now do a POST request

const result = await fetch(
  `https://gitlab.com/api/v4/projects/${process.env.CI_MERGE_REQUEST_PROJECT_ID}/merge_requests/${process.env.CI_MERGE_REQUEST_IID}/notes`,
  {
    method: "POST",
    headers: {
      "PRIVATE-TOKEN": process.env.CI_COMMENT_KEY!,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      body: entireMessage.join("\n"),
    }),
  },
);

if (!result.ok) {
  throw new Error(
    "Failed to create PR Comment. Server responded with Status " +
      result.status +
      " and text " +
      (await result.text()),
  );
}

console.error("done.");
